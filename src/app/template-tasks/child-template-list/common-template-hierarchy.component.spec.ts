import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildTemplateListComponent } from './child-template-list-component';

describe('CommonTemplateHierarchyComponent', () => {
  let component: ChildTemplateListComponent;
  let fixture: ComponentFixture<ChildTemplateListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildTemplateListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildTemplateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
