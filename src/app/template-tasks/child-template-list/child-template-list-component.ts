import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {MessageBoxService} from '../../settings/message-box.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TemplateManagementService} from '../../Services/template-management.service';
import {Template} from '../template.model';
import {ApiError} from '../../settings/api-error.model';

@Component({
  selector: 'app-common-template-hierarchy',
  templateUrl: './child-template-list-component.html',
  styleUrls: ['./child-template-list-component.css']
})
export class ChildTemplateListComponent implements OnChanges {
  public receivedTemplateId: string;
  @Input() setLevel;
  @Output() templateEventSend = new EventEmitter();

  get displayTemplateId() {
    return this.receivedTemplateId;
  }
  @Input()
  set displayTemplateId(value) {
    this.receivedTemplateId = value;
        if (this.receivedTemplateId !== undefined && this.receivedTemplateId !== '') {
      console.log(value);
          setTimeout(() => {this.loadTemplateList(); }, 5 );
          }}
  constructor(private detect: ChangeDetectorRef,
    messageBoxService: MessageBoxService,
    snackBarService: MatSnackBar,
    templateManagementService: TemplateManagementService,
  ) {
    this.messageBoxService = messageBoxService;
    this.templateManagementService = templateManagementService;
      }


  public selectedTemplateId: string;
  private messageBoxService: MessageBoxService;
  private templateManagementService: TemplateManagementService;
  public levelTemplate: Template[] = [];
  onRowClick(template): void {
    this.selectedTemplateId = template.templateId;
    this.templateEventSend.emit({act: 'select', t: template, level: this.setLevel});
  }
  onClickGenerateTask(template) {
    this.templateEventSend.emit({act: 'generate', t: template, level: this.setLevel});
  }
  loadTemplateList() {
   this.templateManagementService.getTemplateById(this.receivedTemplateId, 'children').subscribe(
      {
        next: (template: Template) => {
          this.levelTemplate = template.childTemplates;
        },
        error: (apiError: ApiError) => this.messageBoxService.info('Could not load template',
          apiError.title, apiError.detail)
      });
  }
  onClickEdit(template: Template) {
    this.templateEventSend.emit({act: 'edit', t: template, level: this.setLevel, parentTemplate: this.receivedTemplateId});
  }
  onClickRemove(templateId) {
    this.templateEventSend.emit({act: 'remove', childTemplate: templateId, parent: this.receivedTemplateId, level: this.setLevel});
       }
  onClickCheckSimilar(template) {
       this.templateEventSend.emit({act: 'checkSimilar', oldTemplate: template, parentTemplate: this.receivedTemplateId, level: this.setLevel});
      }
  onClickClone(template) {
    this.templateEventSend.emit({act: 'clone', t: template, level: this.setLevel});
      }
  onAddChildTemplate() {
    this.templateEventSend.emit({act: 'add', parentId: this.receivedTemplateId, level: this.setLevel});
    }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes);
    // this.receivedTemplateId = changes?.displayTemplateId?.currentValue;
    // if (this.receivedTemplateId !== '') {
    // this.loadTemplateList(); }
  }
}
