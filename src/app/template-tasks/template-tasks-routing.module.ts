import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TemplateTasksResolverService} from './template-tasks-resolver.service';
import {BaseTemplateComponent} from './base-template/base-template.component';

const routes: Routes = [
  {
    path: '',
    component: BaseTemplateComponent,
    resolve: {
       taskMaster: TemplateTasksResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateTasksRoutingModule {}
