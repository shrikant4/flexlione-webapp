import {NgModule } from '@angular/core';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TemplateTasksRoutingModule} from './template-tasks-routing.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatOptionModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {TemplateTasksResolverService} from './template-tasks-resolver.service';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {BaseTemplateComponent} from './base-template/base-template.component';
import { SelectTemplateComponent } from './select-template/select-template.component';
import {TemplateFormComponent} from './template-form/template-form.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {GenerateTaskComponent} from './generate-task/generate-task.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ArticleModule} from '../article/article.module';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {ChildTemplateListComponent} from './child-template-list/child-template-list-component'
import {SearchManagementService} from '../Services/search-management.service';


@NgModule({
    imports: [
        SharedModule,
        TemplateTasksRoutingModule,
        MatMenuModule,
        MatIconModule,
        MatListModule,
        CommonModule,
        MatIconModule,
        MatMenuModule,
        MatListModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule,
        MatDialogModule,
        MatInputModule,
        ReactiveFormsModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        ArticleModule,
        MatTableModule,
        MatPaginatorModule,
    ],
  declarations: [
    BaseTemplateComponent,
    SelectTemplateComponent,
    TemplateFormComponent,
    GenerateTaskComponent,
    ChildTemplateListComponent,
     ],
  exports: [
    BaseTemplateComponent
  ],
  providers: [
    MatSnackBar,
    TemplateTasksResolverService,
    SearchManagementService
  ]
})
export class TemplateTasksModule {}
