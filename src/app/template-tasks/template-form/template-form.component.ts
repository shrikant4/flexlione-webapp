import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ProfileManagementService} from '../../Services/profile-management.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Template} from '../template.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProfileStoreModel} from '../../shared/store/interfaces/profile-store.model';
import {UserService} from '../../core';
import {ProfileModel} from '../../profile/profile-models/profile.model';
import {TemplateManagementService} from '../../Services/template-management.service';
import {TaskModel} from '../../shared/task-form/task.model';
import {ActivatedRoute} from '@angular/router';
import {ApiError} from '../../settings/api-error.model';
import {MessageBoxService} from '../../settings/message-box.service';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']

})

export class TemplateFormComponent implements OnInit {
  UserList: ProfileStoreModel[] = [];
  public isEdit: string ;
  Profiles: ProfileStoreModel[] = [];
  currentUser: ProfileModel;
  template: Template;
  templateId: string ;
  public profileId: string;
  task: TaskModel;
  public typeId: string;


  newTemplate: FormGroup;


  // newTemplate: FormGroup = new FormGroup({
  //   'templateId': new FormControl(''),
  //   'owner': new FormControl(''),
  //   'description': new FormControl(''),
  //   'createdAt': new FormControl(''),
  //   'role': new FormControl('', [Validators.required]),
  // });

  public loggedInUserId: string;
  constructor(
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<Template>,
    public snackBarService: MatSnackBar,
    private messageBoxService: MessageBoxService,
    public templateManagementService: TemplateManagementService,
    private profileManagementService: ProfileManagementService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) {

    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
        this.loggedInUserId = this.currentUser.profileId;
      });
    if (this.data.isEdit === true) {
    this.typeId = data.template.templateId ; }
      }

  async ngOnInit() {
    this.dialogRef.updateSize('100%', '90%');

    const profiles = await this.profileManagementService.getAllProfiles().toPromise();
    this.UserList.length = 0;
    for (let i = 0; i < profiles.length; i++) {
      this.UserList.push(profiles[i]);
    }
    if (this.data.isEdit === true) {
      this.newTemplate = this.formBuilder.group(this.data.template);
      this.newTemplate.patchValue({
        createdAt: new Date(),
        }
      );
      console.log(this.newTemplate.value);

    } else {
      this.newTemplate = this.formBuilder.group(new Template());
      this.newTemplate.patchValue({
        templateId: 'server generated',
        createdAt: new Date(),
        owner: this.loggedInUserId,
      });
      this.newTemplate.controls['templateId'].disable();
      this.newTemplate.controls['createdAt'].setValue(new Date());
      this.newTemplate.controls['owner'].setValue(this.loggedInUserId);
    }
    this.newTemplate.controls['templateId'].disable();
    this.newTemplate.controls['createdAt'].disable();
  }

  onSave() {
    this.templateManagementService.createOrUpdateTemplate(this.newTemplate.getRawValue())
      .subscribe({
        next: (template) => {
           this.dialogRef.close(template);
          },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Could not save a Template.',
            apiError.title, apiError.detail);
        }
      });
  }

}
