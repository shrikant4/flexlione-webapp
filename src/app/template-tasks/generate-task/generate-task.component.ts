import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {MessageBoxService} from '../../settings/message-box.service';
import {TaskManagementService} from '../../Services/task-management-service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {ProfileManagementService} from '../../Services/profile-management.service';
import {ProfileStoreModel} from '../../shared/store/interfaces/profile-store.model';
import {Template} from '../template.model';
import {TaskModel} from '../../shared/task-form/task.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TemplateManagementService} from '../../Services/template-management.service';
import {UserService} from '../../Services/user.service';
import {ProfileModel} from '../../profile/profile-models/profile.model';
import {ApiError} from '../../settings/api-error.model';




@Component({
  selector: 'app-generate-task',
  templateUrl: './generate-task.component.html',
  styleUrls: ['./generate-task.component.css']
})

export class GenerateTaskComponent implements OnInit , AfterViewInit {
  public taskIdListForParent: { 'taskId': string, 'description': string }[];
  Profiles: ProfileStoreModel[] = [];
  positionList: string[] = [];
  task: TaskModel;
  mapPair = new Map<string, string>();
  templateId: string;
  generateTask: FormGroup;

  private currentUser: ProfileModel;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private messageBoxService: MessageBoxService,
    public dialogRef: MatDialogRef<Template>,
    private taskManagementService: TaskManagementService,
    private snackBarService: MatSnackBar,
    private router: Router,
    private store: Store<AppState>,
    private profileManagementService: ProfileManagementService,
    private templateManagementService: TemplateManagementService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder
  ) {

    this.templateId = data.templateId;
    this.generateTask = this.formBuilder.group({
      templateId: '',
      taskPrefix: '',
      parentTaskId: '',
      createdAt: '',
      includeReference: false,
      includeAllChildren: false,
      roleProfileMap: this.formBuilder.array([]),
    });

  }

  get roleProfileMap() {
    return this.generateTask.controls['roleProfileMap'] as FormArray;
  }

  addRoleProfilePair(roleP: string) {
    const roleProfile = this.formBuilder.group({
      role: [roleP],
      profile: ['', [Validators.required]],
    });
    this.roleProfileMap.push(roleProfile);
  }



 ngOnInit() {



    this.dialogRef.updateSize('40%', '80%');
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.generateTask.patchValue({
        templateId: this.data.templateId
      },
    );
    this.generateTask.controls['templateId'].disable();

    // array.reduce() method is used to reduce an array into a single value by passing a callback function on each element of an array.
    // The reducer walks through the array element-by-element, at each step adding the current array value to the
    // result from the previous step until there are no more elements to add.

    this.generateTask.controls['createdAt'].setValue(new Date());
    this.generateTask.controls['createdAt'].disable();

    this.getAllProfiles();
    this.getAllRoles();
  }

  getAllRoles() {
    this.templateManagementService.getAllRolesForTemplateId(this.data.templateId)
      .subscribe({
        next: (role) => {
          for (let k = 0; k < role.length; k++) {
            this.positionList.push(role[k]);
            this.addRoleProfilePair(role[k]);

          }
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error.',
            apiError.title, apiError.detail);
        }
      });
  }

  getAllProfiles() {
    this.profileManagementService.getAllProfiles().subscribe({
      next: (prof) => {
        prof.reduce((acc, person) => {
          return {...acc, [person.profileId]: person};
        }, {});
      },
      error: (apiError: ApiError) => {
        this.messageBoxService.info ('Error', apiError.title, apiError.detail);
      }
    });
  }

  async ngAfterViewInit() {
    this.taskManagementService.getTaskIdList('', this.getTaskIdListForParent);
    this.Profiles = await this.profileManagementService.getAllProfiles().toPromise();
  }


//   createTemplateTask(generateTask: FormGroup): Template {
//     const templateTask = new Template();
//     templateTask.templateId = generateTask.getRawValue().templateId;
//     templateTask.taskPrefix = generateTask.getRawValue().taskPrefix;
//     templateTask.parentTaskId = generateTask.getRawValue().parentTaskId;
//     templateTask.includeReference = generateTask.getRawValue().includeReference;
//     templateTask.includeAllChildren = generateTask.getRawValue().includeAllChildren;
//     for (let i = 0;  i < generateTask.getRawValue().roleProfileMap.length; i++ ) {
//     this.mapPair.set(generateTask.getRawValue().roleProfileMap[i].role, generateTask.getRawValue().roleProfileMap[i].profile);
// }
//     // each pair is picked up by reduce function and key value pair is mapped as server required data.
//    templateTask.roleProfileMap = [...this.mapPair].reduce((o, [key, value]) => (o[key] = value, o), {});
//      templateTask.createdAt = this.datePipe.transform(generateTask.getRawValue().createdAt, 'yyyy-MM-dd');
//     templateTask.createdBy = this.currentUser.profileId;
//       return templateTask;
//
  getTaskIdListForParent = (taskIdList: { 'taskId': string, 'description': string }[]) => {
    this.taskIdListForParent = taskIdList;
  }


  onGenerate() {

    const templateTask = this.generateTask.getRawValue();

    for (let i = 0; i < this.generateTask.value.roleProfileMap.length; i++) {
      this.mapPair.set(this.generateTask.value.roleProfileMap[i].role, this.generateTask.value.roleProfileMap[i].profile);
    }
    // each pair is picked up by reduce functuin and key value pair is mapped as server required data.
    templateTask.roleProfileMap = [...this.mapPair].reduce((o, [key, value]) => (o[key] = value, o), {});

    this.templateManagementService.generateTaskFromTemplate(templateTask)
      .subscribe({
        next: (task) => {
          console.log(task);
          this.snackBarService.open('Success. New task  has been  created.', '', {duration: 3000});
          this.dialogRef.close();
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Error in generating Task',
            apiError.title, apiError.detail);
        }
      });
  }
}
