import {Component, Inject, OnInit} from '@angular/core';
import {Template} from '../template.model';
import {MessageBoxService} from '../../settings/message-box.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {ProfileManagementService} from '../../Services/profile-management.service';
import {ProfileStoreModel} from '../../shared/store/interfaces/profile-store.model';
import {TemplateFormComponent} from '../template-form/template-form.component';
import {TemplateManagementService} from '../../Services/template-management.service';
import {ApiError} from '../../settings/api-error.model';
import {PageEvent} from '@angular/material/paginator';
import {SearchManagementService} from '../../Services/search-management.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-select-template',
  templateUrl: './select-template.component.html',
  styleUrls: ['./select-template.component.css']
})
export class SelectTemplateComponent implements OnInit {
  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageIndex = +1;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  templateSearch: FormGroup;

  // MatPaginator Output
  pageEvent: PageEvent;
  public selectedTemplateId: string;
  public template: Template;
  private messageBoxService: MessageBoxService;
  private snackBarService: MatSnackBar;
  public templatePool: Template[];
  private profileData: ProfileStoreModel[];
  public templateManagementService: TemplateManagementService;
  constructor(public dialog: MatDialog,
              private router: Router,
              templateManagementService: TemplateManagementService,
              messageBoxService: MessageBoxService,
              public dialogRef: MatDialogRef<Template>,
              snackBarService: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public profileManagementService: ProfileManagementService,
              private  searchManagementService: SearchManagementService,
              private fromBuilder: FormBuilder) {
        this.messageBoxService = messageBoxService;
        this.snackBarService = snackBarService;
        this.templateManagementService = templateManagementService;
  }



  ngOnInit(): void {
    this.templateSearch = this.fromBuilder.group( new Template());
    this.loadTemplatePool();
    this.dialogRef.updateSize('100%', '100%');

    this.profileManagementService.getAllProfiles()
        .subscribe({
          next: (profiles) => {this.profileData = profiles;
          }
        });
  }

  loadTemplatePool() {
    let tId = '';
    if (this.data.action === 'checkSimilar') {tId = this.data.actionId; }
    if (this.data.action === 'addChild') { tId = ''; }
    this.templateManagementService.getSimilarTemplateList(tId, this.pageIndex, this.pageSize)
      .subscribe( {next: (templates) => {
        for (let i = 0; i < templates.length; i++) {
          templates[i].owner = this.getUserName(templates[i].owner);
        }

        this.templatePool = templates; },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Error in loading templates .',
            apiError.title, apiError.detail);
        }
      } );

  }

  getUserName(userId) {
    const profile = this.profileData.filter(function (value) {
      return (value.profileId === userId);
    });
    return profile[0] === undefined ? userId : profile[0].name;
     }

  selectedTemplate(template) {
    this.selectedTemplateId = template.templateId;

  }

  onAddClick() {
    this.dialogRef.close(this.selectedTemplateId);
  }

  onReplace() {
    const a = {};
    a['templateId'] = this.selectedTemplateId;
    a['templateAction'] = 'replace';
    this.dialogRef.close(a);
  }

  onCheckout() {
    const a = {};
    a['templateId'] = this.selectedTemplateId;
    a['templateAction'] = 'checkout';
    this.dialogRef.close(a);
      }

  onCreatNewTemplate() {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();

    dialogConfig.data = {

      isEdit: false,
    };
    this.dialog.open(TemplateFormComponent, dialogConfig).afterClosed()
      .subscribe({
        next: (newTemplate: Template) => {
          if (newTemplate !== undefined ) {
            this.snackBarService.open('Template ' + newTemplate.templateId + ' is created', '', {duration: 3000});
          }
          this.loadTemplatePool();
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Error in creating New Template',
            apiError.title, apiError.detail);
        }
      });
  }

  updateList(pageEvent: PageEvent) {
    this.pageIndex = pageEvent.pageIndex + 1;
    this.pageSize = pageEvent.pageSize;
      this.loadTemplatePool();
  }


  searchTemplates(description: string, pageEvent: PageEvent) {
    const pageIndex = pageEvent.pageIndex + 1;
    const pageSize = pageEvent.pageSize;
    this.searchManagementService.getSearchResultForTemplate(description, pageIndex, pageSize).
    subscribe({
      next: (search) => {
        this.templatePool = search;
    },
      error: (apiError: ApiError) => {
        this.messageBoxService.info('Error: Could not sent request',
          apiError.title, apiError.detail); }
    });
    // this.updateList(this.pageEvent);
  }
}
