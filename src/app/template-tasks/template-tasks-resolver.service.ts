import { Injectable, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {  ArticlesService, UserService } from '../core';
import { catchError } from 'rxjs/operators';
import {TaskMaster} from '../models/master.model';
import {TemplateManagementService} from '../Services/template-management.service';

@Injectable()
export class TemplateTasksResolverService implements Resolve<TaskMaster> {
  constructor(
    private articlesService: ArticlesService,
    private router: Router,
    private userService: UserService,
    private templateManagementService: TemplateManagementService,
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {

    return this.templateManagementService.getTemplateById('2', 'child').
    pipe(catchError((err) => this.router.navigateByUrl('/')));
  }
}
