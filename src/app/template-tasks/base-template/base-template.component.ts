import {Component, OnInit} from '@angular/core';
import {Template} from '../template.model';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {MessageBoxService} from '../../settings/message-box.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SelectTemplateComponent} from '../select-template/select-template.component';
import {TemplateFormComponent} from '../template-form/template-form.component';
import {TemplateManagementService} from '../../Services/template-management.service';
import {GenerateTaskComponent} from '../generate-task/generate-task.component';
import {ApiError} from '../../settings/api-error.model';

@Component({
  selector: 'app-base-template',
  templateUrl: './base-template.component.html',
  styleUrls: ['./base-template.component..css']

})

export class BaseTemplateComponent implements OnInit {
  levelReceived: string;
  templateIdSelected: string;
  eventUpdateLevel: string;
  eventUpdateId: string;
  public checkedOutTemplate: Template;
  public selectedTemplateId: string;
  public checkedOutTemplateId: string;
  public defaultCheckedoutTemplate = '1';
  public template: Template;
  private messageBoxService: MessageBoxService;
  private snackBarService: MatSnackBar;

  constructor(public dialog: MatDialog,
              private router: Router,
              public templateManagementService: TemplateManagementService,
              messageBoxService: MessageBoxService,
              snackBarService: MatSnackBar,
                            ) {
    this.messageBoxService = messageBoxService;
    this.snackBarService = snackBarService;
  }
  sendL0TemplateId: string;
  sendL1TemplateId: string;
  sendL2TemplateId: string;
  sendL3TemplateId: string;


  getTemplate(templateId) {
         return  this.templateManagementService.getTemplateById(templateId, 'children')
       .toPromise();

  }

  ngOnInit() {
    this.checkoutTemplateId (this.defaultCheckedoutTemplate);
     }

  onRowClick(templateId: string) {
    this.selectedTemplateId = templateId;
    console.log(this.selectedTemplateId); }


  onClickEdit(template: Template, level, parentTemplate) {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      template : template,
      isEdit: true,
    };
     this.dialog.open(TemplateFormComponent, dialogConfig).afterClosed()
      .subscribe({
        next: (newTemplate: Template) => {
          if (newTemplate !== undefined) {
            this.snackBarService.open('Template ' + newTemplate.templateId + ' updated', '', {duration: 3000});
          this.autoRefresh(level, parentTemplate);
          }
                  },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Failed to update Template.',
            apiError.title, apiError.detail);
        }
        });
  }

  onClickCheckSimilar(oldTemplateId, parentTemplateId, level, call: string) {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();

        dialogConfig.data = {
          actionId: oldTemplateId,
          action: 'checkSimilar',
          parentId: this.checkedOutTemplateId
        };
    const dialogRef = this.dialog.open(SelectTemplateComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((a: any) => {
      if ( a !== undefined) {
        if (a.templateAction === 'replace') {

// Replace template if called from Checkout template, call = "base" and if called by any other component, call = "component"
      switch (call) {
            case 'component': {
          this.templateManagementService.replaceChildTemplate(oldTemplateId, a.templateId, parentTemplateId)
            .subscribe({
              next: (templateA) => {
                this.snackBarService.open('Template ' + templateA.templateId + ' replaced.', '', {duration: 3000});

                this.autoRefresh(level, parentTemplateId);
               },
              error: (apiError: ApiError) => {
                this.messageBoxService.info('Error: Failed to replace Template.',
                  apiError.title, apiError.detail);
              }
            });
              break; }
            case 'base': {
              this.checkoutTemplateId(a.templateId);
            }
          }
        } else if (a.templateAction === 'checkout') {
          this.checkoutTemplateId(a.templateId);
          }
      }
    }
        );

    }

  onClickClone(template) {
     this.templateManagementService.cloneTemplate(template.templateId)
      .subscribe( {next: (temp) => {this.snackBarService.open('new clone is ' + temp.templateId, '', { duration: 3000 });
         },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Error in cloning Template .',
            apiError.title, apiError.detail);
        }
      });
  }

  checkoutTemplateId (templateId) {
      this.getTemplate(templateId)
        .then((temp: Template) => {this. checkedOutTemplate = temp; });
      this.autoRefresh('0', templateId );

      // this.sendL0TemplateId = templateId;
      // this.sendL1TemplateId = '';
      // this.sendL2TemplateId = '';
      // this.sendL3TemplateId = '';
          }



  toggle() {
    this.router.navigateByUrl('/task-tree');
  }

  onClickGenerateTask(templateId) {
        const dialogConfig: MatDialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      templateId: templateId
    };
    this.dialog.open(GenerateTaskComponent, dialogConfig).afterClosed()
      .subscribe({
        next: (newTemplate: Template) => {
        console.log(newTemplate); }
      });
  }

  onAddChildTemplate(parentId, level) {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();

    dialogConfig.data = {

      actionId: parentId,
      action: 'addChild',
    };
    const dialogRef = this.dialog.open(SelectTemplateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((selectedTemplateId: string) => {
      if (selectedTemplateId !== undefined) {
        this.templateManagementService.addChildTemplate(selectedTemplateId, parentId )
          .subscribe((template) => {
            this.snackBarService.open('Child Template added successfully.', '', {duration: 3000});
          this.autoRefresh(level, parentId);
          });
      }
    });
  }

  onClickRemove(child, parent, level) {
    this.messageBoxService.confirmWarn(
      'Are you sure ?')
      .afterClosed().subscribe({

      next: (proceed: boolean) => {
        if (proceed) {
          this.templateManagementService.removeChildTemplate(child, parent)
            .subscribe({
              next: (temp) => {
                this.snackBarService.open('Template Removed.', '', { duration: 1000 });
                this.autoRefresh(level, temp.templateId);            },
              error: (apiError: ApiError) => {
                this.messageBoxService.info('Error: Could not remove Template.',
                  apiError.title, apiError.detail);
              }
            });
        }
      }
    });
  }

  levelAndIdReceivedFromChild(template, level) {
    this.templateIdSelected = template.templateId;
    this.levelReceived = level;
        this.autoRefresh((parseInt(this.levelReceived, 10)   + 1).toString(), this.templateIdSelected );
      }

  autoRefresh(level, id ) {
    // we are passing string value to refresh the child, Because any operation done on same child so same
    // primitive type Id will be passed & will lead to fail change detection. To solve the first a '0' id is passed and in 1 ms again correct id will  pass
    // in child again. 5 ms time delay is added to update, so no error will be triggered apart from change.
    // further to simplify code one single function is used to refresh the child as well as load templates on child.
        this.eventUpdateLevel = level;
        this.eventUpdateId = '0';
        this.sendEventUpdate();
        setTimeout(() => {this.eventUpdateId = id;
                                this.sendEventUpdate(); }, 1 );
        }
  sendEventUpdate() {
     switch (this.eventUpdateLevel) {
      case '0': {
        this.sendL0TemplateId = this.eventUpdateId;
        this.sendL1TemplateId = '';
        this.sendL2TemplateId = '';
        this.sendL3TemplateId = '';
        break;
      }
      case '1': {
        this.sendL1TemplateId = this.eventUpdateId;
        this.sendL2TemplateId = '';
        this.sendL3TemplateId = '';
        break;
      }
      case '2': {
        this.sendL2TemplateId = this.eventUpdateId;
        this.sendL3TemplateId = '';
        break;
      }
      case '3': {
        this.sendL3TemplateId = this.eventUpdateId;
        break;
      }
       case '4': {
         this.sendL1TemplateId = this.sendL3TemplateId;
         this.sendL2TemplateId = '';
         setTimeout(() => {this.sendL3TemplateId = ''; }, 2 );
// to use single function & to avoid errors 2 ms time delay is added which will allow to refresh L1 first in case of L3 is clicked.
         break;
       }
          }}

  templateEventReceived($event: any) {
    switch ($event.act) {
      case 'edit': { this.onClickEdit($event.t, $event.level, $event.parentTemplate); break; }
      case 'clone': { this.onClickClone($event.t); break; }
      case 'select': { this.levelAndIdReceivedFromChild($event.t, $event.level); break; }
      case 'generate': { this.onClickGenerateTask($event.t.templateId); break; }
      case 'checkSimilar': { this.onClickCheckSimilar($event.oldTemplate.templateId, $event.parentTemplate, $event.level, 'component'); break; }
      case 'add': { this.onAddChildTemplate($event.parentId, $event.level ); break; }
      case 'remove': { this.onClickRemove($event.childTemplate, $event.parent, $event.level ); break; }

    }
  }


}

