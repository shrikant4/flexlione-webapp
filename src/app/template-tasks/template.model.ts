


export class  Template {
  templateId = '';

  description = '';

  childTemplateIds: string[];

  childTemplates: Template [];
  owner = '';

  createdBy: string ;

  createdAt = '';

  position: string;

  profile: string ;

  taskPrefix: string ;

  parentTaskId: string;
  role = '';
  name: string;

  includeReference: boolean ;

  includeAllChildren: boolean;

  roleProfileMap: any;

  }
