import {  NgModule } from '@angular/core';
import { ArticleComponent } from './article.component';
import { ArticleCommentComponent } from './article-comment/article-comment.component';
import { ArticleResolver } from './article-resolver.service';
import { MarkdownPipe } from './markdown.pipe';
import { SharedModule } from '../shared';
import { ArticleRoutingModule } from './article-routing.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {TaskFormComponent} from '../shared/task-form/task-form.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {ViewDependencyComponent} from './dependency/view-dependency/view-dependency.component';
import {AddOrEditDependencyDialogComponent} from './dependency/add-edit-dependency/add-or-edit-dependency-dialog.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {TimeCalculationComponent} from './time-calculation/time-calculation.component';
import {MatTableModule} from '@angular/material/table';
import {FormsModule} from '@angular/forms';
import {TaskSessionSummaryComponent} from '../shared/task-session-summary/task-session-summary.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';


@NgModule({
  imports: [
    SharedModule,
    ArticleRoutingModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatSelectModule,
    MatInputModule,
    MatNativeDateModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatInputModule,
    MatExpansionModule,
    MatTableModule,
    FormsModule,
    MatChipsModule,
    MatAutocompleteModule,

  ],
    declarations: [
        ArticleComponent,
        ArticleCommentComponent,
        MarkdownPipe,
        TaskFormComponent,
      ViewDependencyComponent,
      AddOrEditDependencyDialogComponent,
      TimeCalculationComponent,
    ],
  exports: [
    TaskFormComponent,
    TaskSessionSummaryComponent,
      ],


    providers: [
      { provide: MAT_DIALOG_DATA, useValue: {} },
        {
            provide: MatDialogRef,
            useValue: {}
        },
        ArticleResolver,
        MatSnackBar
    ]
})
export class ArticleModule {}
