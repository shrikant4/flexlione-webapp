import {AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { ActivatedRoute, Router} from '@angular/router';
import {TaskModel} from '../shared/task-form/task.model';
import {AddOrEditScheduleDialogComponent} from '../profile/schedule/add-or-edit-schedule-dialog.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
// @ts-ignore
import { ArticlesService, CommentsService, UserService} from '../core';
import {TaskManagementService} from '../Services/task-management-service';
import {MessageBoxService} from '../settings/message-box.service';
import {CommentManagementService} from '../Services/comment-management.service';
import {TaskComment} from './article-comment/task-comment.model';
import {DatePipe} from '@angular/common';
import {TaskHierarchyManagementService} from '../Services/task-hierarchy-management.service';
import {TaskHierarchyModel} from '../tasks-hierarchy/task-hierarchy.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProfileModel} from '../profile/profile-models/profile.model';
import {TaskScheduleModel} from '../profile/schedule/task-schedule.model';
import {ApiError} from '../settings/api-error.model';
import {ProfileManagementService} from '../Services/profile-management.service';
import {ProfileStoreModel} from '../shared/store/interfaces/profile-store.model';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Observable} from 'rxjs';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {map, startWith, tap} from 'rxjs/operators';


@Component({
  selector: 'app-article-page',
  templateUrl: './article.component.html',
  styleUrls: ['article.component.css']
})
export class ArticleComponent implements OnInit {

  task: TaskModel;
  currentUser: ProfileModel;
  comments: TaskComment[] = [];
  commentControl = new FormControl();
  isSubmitting = false;
  TaskForm: FormGroup;
  TaskHierarchy: TaskHierarchyModel = new TaskHierarchyModel();
  Profiles: ProfileStoreModel[];
  public loggedInUserId: string;


  removable = true;
  labelCtrl = new FormControl();
  chipListCtrl = new FormControl('');
  filteredLabels: Observable<string[]>;
  allLabels: string[] = [ 'sprint'];
  headerText = '';

  @ViewChild('labelInput') labelInput: ElementRef<HTMLInputElement>;



  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private articlesService: ArticlesService,
    private commentsService: CommentsService,
    private router: Router,
    private userService: UserService,
    private taskManagementService: TaskManagementService,
    private messageBoxService: MessageBoxService,
    private  commentManagementService: CommentManagementService,
    private datepipe: DatePipe,
    private  taskHierarchyManagementService: TaskHierarchyManagementService,
    private  snackBarService: MatSnackBar,
    private profileManagementService: ProfileManagementService,
    private formBuilder: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef
  ) {

    // Retreive the prefetched article
    this.route.data.subscribe(
      (data: { article: TaskModel }) => {
        this.task = data.article;
        this.TaskForm = this.formBuilder.group(data.article);
        this.TaskForm.addControl('hrsSpentTillNow', new FormControl());
        this.TaskForm.controls['taskId'].disable();
        this.TaskForm.controls['hrsSpentTillNow'].disable();
        this.TaskForm.controls['createdAt'].setValue(new Date());
        this.TaskForm.controls['createdAt'].disable();
        this.TaskForm.addControl('chipListCtrl', this.chipListCtrl);

        console.log(this.task);
        this.comments = [];
        // Load the comments on this article
        this.populateComments();
      }
    );
    this.filteredLabels = this.labelCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => fruit ? this._filterLabels(fruit) : this.allLabels.slice()));
  }




  ngOnInit() {
    this.profileManagementService.getAllProfiles().
    subscribe((prof) => {this.Profiles = prof; }
    );

      // Load the current user's data
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
        this.loggedInUserId = userData.profileId;
      }
    );


    // Receiving estimated hours and spent hours separately and then plugging into taskform
    this.taskHierarchyManagementService.getTaskHierarchyByTaskId(this.task.taskId, 'children', this.onSuccess);
    this.taskManagementService.getTaskById(this.task.taskId, '').subscribe({
      next: ( task) => {
        console.log(task.label);
        this.TaskForm.controls['chipListCtrl'].setValue(task.label);
      },
      error: (apiError: ApiError) => {
        this.messageBoxService.info('Error: Task not created .', apiError.title, apiError.detail);
      }
    });
    this.changeDetectorRef.detectChanges();



  }

  public onSuccess = (taskHierarchy: TaskHierarchyModel) => {
    this.snackBarService.open('Task Hierarchy Successfully received' , '', {duration: 300});
   this.TaskHierarchy = taskHierarchy;
    this.TaskForm.controls['hrsSpentTillNow'].setValue(taskHierarchy.totalHoursSpent);
  }

  seeInTaskTree() {
    this.taskManagementService.getTaskById(this.task.parentTaskId, 'children').subscribe({
      next: ( parentTask) => {
       this.navigateToTaskHierarchy(parentTask);
      },
      error: (apiError: ApiError) => {
        this.messageBoxService.info('Error: Task not created .', apiError.title, apiError.detail);
      }
    }); }

  navigateToTaskHierarchy(parentTask: TaskModel) {
    if ( parentTask.taskId === '0') {
      this.router.navigateByUrl('/task-tree?L0=' + this.task.taskId);
      return;
    }
    if (parentTask.parentTaskId === '0') {
      this.router.navigateByUrl('/task-tree?L0=' + parentTask.taskId + '&L1=' + this.task.taskId);
      return;
    }
    this.router.navigateByUrl('/task-tree?L0=' + parentTask.parentTaskId + '&L1=' + this.task.parentTaskId
      + '&L2=' + this.task.taskId);
  }

  goToParentTask() {
      this.router.navigateByUrl('/article/' + this.task.parentTaskId);
  }



  populateComments() {
    this.commentManagementService.getCommentsByTaskId(this.task.taskId).subscribe({
      next: (commentList) => {
       this.comments.push.apply(this.comments, commentList);
      },
      error: () => { }
    });
  }

  addComment() {
    const commentBody = this.commentControl.value;
    const newComment: TaskComment = {
      commentId: 'next',
      taskId: this.task.taskId,
      createdBy: this.currentUser.profileId,
      message: commentBody,
      createdAt: this.datepipe.transform(Date.now(), 'yyyy-MM-dd')};
     this.commentControl.reset('');
    this.comments = [];

    this.commentManagementService.createOrUpdateComment(newComment).subscribe({
      next: (comment) => {
       console.log(comment);
       this.snackBarService.open('Comment Successfully logged', '', {duration: 3000});
        this.populateComments();
       },
      error: (apiError: ApiError) => {this.messageBoxService.info('Error in logging comment', apiError.title, apiError.detail); }
    });
  }

  onDeleteComment(comment) {
    this.commentsService.destroy(comment.commentId, this.task.taskId)
      .subscribe(
        success => {
          this.comments = this.comments.filter((item) => item !== comment);
        }
      );
  }


  onAddToSchedule(task: TaskModel): void {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      task : task
    };
    this.dialog.open(AddOrEditScheduleDialogComponent, dialogConfig)
      .afterClosed().subscribe({
      next: (taskSchedule: TaskScheduleModel) => {},

      error: () => {}
    });
  }

  onAddSiblingTask(task: TaskModel) {
    this.headerText = 'Sibling of task ID: ';
    this.TaskForm = this.formBuilder.group(task);
    this.TaskForm.patchValue({
      taskId: 'server generated',
      description: '',
      createdAt: new Date(),
      expectedHours: 0,
    });
    this.TaskForm.controls['taskId'].disable();
    this.TaskForm.controls['createdAt'].disable();
    this.changeDetectorRef.detectChanges();
     }

  onAddChildTask(task: TaskModel) {
    this.headerText = 'Child of task ID: ';
    this.TaskForm = this.formBuilder.group(task);
    this.TaskForm.patchValue({
      parentTaskId: task.taskId,
      taskId: 'server generated',
      description: '',
      createdAt: new Date(),
      expectedHours: 0,
    });
    this.TaskForm.controls['taskId'].disable();
    this.TaskForm.controls['createdAt'].disable();
    this.changeDetectorRef.detectChanges();

    }

  remove(label: string): void {
    const index = this.chipListCtrl.value.indexOf(label);

    if (index >= 0) {
      const val = [...this.chipListCtrl.value];
      val.splice(index, 1);
      this.chipListCtrl.setValue(val);
    }
  }

  onNewChipSelected(event: MatAutocompleteSelectedEvent): void {
    event.option.deselect();
    const val = [...this.chipListCtrl.value];
    val.push(event.option.viewValue);
    this.chipListCtrl.setValue(val);
    this.labelInput.nativeElement.value = '';
    this.labelCtrl.setValue(null);
  }

  private _filterLabels(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allLabels.filter(label => label.toLowerCase().indexOf(filterValue) === 0);
  }

  saveLabels(task: TaskModel) {
    this.taskManagementService.addLabelToTask(task.taskId, this.chipListCtrl.value).subscribe({
      next: (label) => {
        console.log(label);
        this.snackBarService.open('Label added Successfully', '', {duration: 3000});
      },
      error: (apiError: ApiError) => {this.messageBoxService.info('Error in adding Labels', apiError.title, apiError.detail); }
    });
  }

  // This function is called  each time the focus(cursor)  is moved out of the MatChip input Box
  addOnBlur(event: FocusEvent) {
    // We do not need it
  }

  // This function is called  each time after adding a chip, next time we click on any element
  onClickAfterAddingChip(event: MatChipInputEvent): void {
    // We do not need it
  }
}
