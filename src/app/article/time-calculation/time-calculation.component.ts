import {ActivatedRoute} from '@angular/router';
import {TaskModel} from '../../shared/task-form/task.model';
import {Component, Input} from '@angular/core';
import {DailyPlanSummaryService} from '../../Services/daily-plan-summary.service';
import {TaskSummaryModel} from '../../profile/daily-plan-summary/task-summary/task-summary.model';
import {TaskHierarchyModel} from '../../tasks-hierarchy/task-hierarchy.model';

@Component({
  selector: 'app-article-time-calculation',
  templateUrl: './time-calculation.component.html',
  styleUrls: ['../article.component.css']
})

export class TimeCalculationComponent {
  task: TaskModel;
  @Input() TaskHierarchyList: TaskHierarchyModel[];
  TaskSummaryList: TaskSummaryModel[];

  constructor(
    private route: ActivatedRoute,
    private dailyPlanSummaryService: DailyPlanSummaryService
  ) {
  }

  public GetTaskSummaryForTaskId(taskId: string) {
    this.dailyPlanSummaryService.getTaskSummaryByTaskId(taskId, 'allChildren', this.onSuccess);
  }

  onSuccess = (taskSummaryList: TaskSummaryModel[]) => {
    this.TaskSummaryList = taskSummaryList;
    this.TaskSummaryList.forEach(function (value) {
     });
  }
}

