import {TaskModel} from '../../shared/task-form/task.model';
export class Dependency {
  dependencyId: string;
  taskId: string;
  dependentTaskId: string;
  description: string;
  taskDetailEditModel: TaskModel;
}
