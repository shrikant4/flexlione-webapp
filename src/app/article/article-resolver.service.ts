import { Injectable, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Article, ArticlesService, UserService } from '../core';
import {catchError, map} from 'rxjs/operators';
import {TaskManagementService} from '../Services/task-management-service';


@Injectable()
export class ArticleResolver implements Resolve<Article> {
  constructor(
    private articlesService: ArticlesService,
    private router: Router,
    private userService: UserService,
    private taskManagementService: TaskManagementService,
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.taskManagementService.getTaskById(route.params['slug'], 'children')
      .pipe(
        map(
          article => {
            if (this.userService.getCurrentUser().name !== undefined) {
              return article;
            } else {
              return article;
            }
          }
        ),
        catchError(() => this.router.navigateByUrl('/'))
      );
  }

}
