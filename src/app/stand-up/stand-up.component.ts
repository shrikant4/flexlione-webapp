import {AfterViewInit, Component, OnInit, Output} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticlesService, UserService} from '../core';
import {ProfileStoreModel} from '../shared/store/interfaces/profile-store.model';
import {DailyPlanSummaryService} from '../Services/daily-plan-summary.service';

import { TaskSummaryModel} from '../profile/daily-plan-summary/task-summary/task-summary.model';
import {ApiError} from '../settings/api-error.model';
import {MessageBoxService} from '../settings/message-box.service';
import {ProfileManagementService} from '../Services/profile-management.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {ProfileModel} from '../profile/profile-models/profile.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import EventEmitter from 'events';
import {Observable} from 'rxjs';
import * as Util from 'util';

@Component({
  selector: 'app-stand-up-page',
  templateUrl: './stand-up.component.html',
  styleUrls: ['stand-up.component.css']
})
export class StandUpComponent implements OnInit {
  message: string;
  @Output() public UploadFinished = new EventEmitter();
  constructor(
    private articlesService: ArticlesService,
    private route: ActivatedRoute,
    private router: Router,
    private dailyPlanSummaryService: DailyPlanSummaryService,
    private  messageBoxService: MessageBoxService,
    private  profileManagementService: ProfileManagementService,
    private datePipe: DatePipe,
    private userService: UserService,
    private  snackBarService: MatSnackBar,
  ) {
  }

  public autoStopMiliSec = 7200000;
  public greetMessage: string;
  public greetTime: number;
  public currentTaskSummary;
  public TaskSummaryList: TaskSummaryModel[] = [];
  public sortedTaskSummaryList: TaskSummaryModel[] = [];
  public Profiles: ProfileStoreModel[] = [];
  options: string[] = [];
  public profileId: string;
  public loggedInId: string;
  public Name: string ;
  newDate: FormGroup = new FormGroup({
    newDate1: new FormControl(new Date(), [Validators.required]),
  });
  currentUser: ProfileModel;
  totalExpectedHr = 0;
  totalActualHr = 0;


  ngOnInit() {

    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
        this.Name = this.currentUser.name;
        this.profileId = this.currentUser.profileId;
        this.loggedInId = this.currentUser.profileId;
      }
    );

    this.GetDailyTaskSummary(this.currentUser.profileId, this.datePipe.transform(this.newDate.getRawValue().newDate1, 'yyyy-MM-dd'));

    this.getAllProfiles();

    this.greetTime = new Date().getHours();
    if (this.greetTime < 12) {
      this.greetMessage = 'Good morning';
    } else if (this.greetTime < 15) {
      this.greetMessage = 'Good afternoon';
    } else if (this.greetTime > 15) {
      this.greetMessage = 'Good evening';
    }



  }

  autoStopFunction() {
    const runningTaskSummary = this.TaskSummaryList.filter(s => s.action === 'start')[0];
    if (runningTaskSummary != null) {

      console.log( 'task running from ' +  (new Date().valueOf() - Date.parse(runningTaskSummary.stamp) ) / 60000 + 'min' );

      if ((new Date().valueOf() - Date.parse(runningTaskSummary.stamp)) > this.autoStopMiliSec) {

        this.updateTaskSummary(runningTaskSummary.taskSummaryId, 'stop', new Date(Date.parse(runningTaskSummary.stamp) + this.autoStopMiliSec ).toISOString())
          .subscribe({
            next: (Summary) => {
              this.snackBarService.open('Task Summary ' + Summary[0].taskSummaryId + ' auto stopped at ' + Summary[0].stamp.slice(11 , 19), 'ok');
              //         this.GetDailyTaskSummary(this.profileId, Summary[0].stamp.slice(0 , 10));
            },
            error: (apiError: ApiError) => {this.messageBoxService.info('Task summary not auto updated',
              apiError.title, apiError.detail); }
          });


      } else {
        setTimeout(() => {

          this.dailyPlanSummaryService.getTaskSummaryById(runningTaskSummary.taskSummaryId)
            .subscribe({
              next: (taskSum) => {
                if (taskSum.action !== 'stop') {

                  this.onClickStop(runningTaskSummary);
                }
              } });

        }, this.autoStopMiliSec - (new Date().valueOf() - Date.parse(runningTaskSummary.stamp)));
      }
    }
  }

  onDateChange(event: MatDatepickerInputEvent<Date | null>) {
    console.log(event.value);
    this.GetDailyTaskSummary(this.profileId, this.datePipe.transform(event.value, 'yyyy-MM-dd'));



  }

  private GetDailyTaskSummary(profileId: string, date: string) {
    this.dailyPlanSummaryService.getDailyTaskSummary(profileId, date).subscribe({
      next : (taskSummaryList) => {
        this.TaskSummaryList = taskSummaryList;
        this.sortedTaskSummaryList = this.TaskSummaryList.sort((a, b) => (a.taskSchedule.startHour - b.taskSchedule.startHour ));
        this.totalExpectedHr = 0;
        this.TaskSummaryList.forEach(item => { if (item.taskSchedule.isPlanned === true) {this.totalExpectedHr += item.expectedHour; } });
        this.totalActualHr = 0;
        this.TaskSummaryList.forEach(item => { this.totalActualHr += item.systemHours; });

        console.log(taskSummaryList);
        this.autoStopFunction();
      },
      error : (apiError: ApiError) => {this.messageBoxService.info('Error in getting Task Summary List',
        apiError.title, apiError. detail);
      }
    });
  }

  getAllProfiles() {
    this.profileManagementService.getAllProfiles()
      .subscribe((profiles) => {
        this.Profiles = profiles;
        for (let i = 0; i <   this.Profiles.length; i++ ) {
          this.options.push( this.Profiles[i].name);
        }
      });
  }

  protected GetProfileName(profileId: string): string {
    const profile = this.Profiles.filter(function (value) {
      return (value.profileId === profileId);
    });
    return profile[0] === undefined ? profileId : profile[0].name;
  }

  private GetProfileId(profileName: string): string {
    const profile = this.Profiles.filter(function (value) {
      return (value.name === profileName);
    });
    return profile[0] === undefined ? profileName : profile[0].profileId;
  }

  public  updateProfile(profileName: string) {
    this.profileId =  this.GetProfileId(profileName);
    this.Name = this.GetProfileName(this.profileId);
    this.GetDailyTaskSummary(this.profileId,  this.datePipe.transform(this.newDate.getRawValue().newDate1, 'yyyy-MM-dd'));
  }

  onClickStart(taskSummary: TaskSummaryModel) {
    console.log(new Date(new Date().valueOf()).toISOString());

    this.updateTaskSummary(taskSummary.taskSummaryId, 'start', new Date(new Date().valueOf()).toISOString())

      .subscribe({
        next: (summary) => {
          this.snackBarService.open('Task Summary ' + summary[0].taskSummaryId + ' started at ' + summary[0].stamp.slice(11, 19), '', {duration: 3000});

          this.GetDailyTaskSummary(this.profileId, summary[0].stamp.slice(0, 10));
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Task summary not updated',
            apiError.title, apiError.detail);
        }
      });

    setTimeout(() => {

      this.dailyPlanSummaryService.getTaskSummaryById(taskSummary.taskSummaryId)
        .subscribe({
          next: (taskSum) => {
            if (taskSum.action !== 'stop') {

              this.onClickStop(taskSummary);
              // this.snackBarService.open('Please restart the task', 'ok', {duration: 3000});
            }
          } });

    }, this.autoStopMiliSec);

  }

  onClickStop(taskSummary: TaskSummaryModel) {
    this.updateTaskSummary(taskSummary.taskSummaryId, 'stop', new Date(new Date().valueOf() ).toISOString())
      .subscribe({
        next: (Summary) => {
          console.log(Summary);
          this.snackBarService.open('Task Summary ' + Summary[0].taskSummaryId + ' stopped at ' + Summary[0].stamp.slice(11 , 19), '', {duration: 4000});
          console.log(Summary[0].stamp);
          this.GetDailyTaskSummary(this.profileId, Summary[0].stamp.slice(0 , 10));
        },
        error: (apiError: ApiError) => {this.messageBoxService.info('Task summary not updated',
          apiError.title, apiError.detail); }
      });
  }

  public selectTaskSummary(event: any, item: TaskSummaryModel) {

    this.currentTaskSummary = item.taskSummaryId;
  }



  updateTaskSummary ( taskSummaryId: string, action: string, stamp: string) {

    let particularTaskSummary: TaskSummaryModel;
    particularTaskSummary = this.TaskSummaryList.filter(x => x.taskSummaryId === taskSummaryId)[0];
    particularTaskSummary.stamp = stamp;
    particularTaskSummary.action = action;

    console.log(stamp);
    return this.dailyPlanSummaryService.UpdateDailyTaskActualTime(this.profileId, particularTaskSummary, this.loggedInId);
    // .subscribe({
    //   next: (taskSummary) => {
    //     this.GetDailyTaskSummary(this.profileId, taskSummary[0].stamp.slice(0, 10));
    //     taskSummary[0] = this.returnedTaskSummary;
    //   },
    //   error: (apiError: ApiError) => {
    //     this.messageBoxService.info('Task summary not updated',
    //       apiError.title, apiError.detail);
    //   }
    //
    // });


  }



}

