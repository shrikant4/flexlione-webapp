import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../core';
import { map ,  take } from 'rxjs/operators';

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private userService: UserService
  ) {
  }
    // canActivate guard is used to prevent access to users who are not authorized to access a route.
    // here if user has permission(authenticated), he will be redirected to Home page


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.userService.isAuthenticated.pipe(take(1), map(isAuth => {

      if (isAuth) {
        this.router.navigateByUrl('/home');
      } else {
        return !isAuth;
      }
    }
    ));
  }
}
