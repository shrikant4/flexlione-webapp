import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Errors, UserService } from '../core';
import {ProfileManagementService} from '../Services/profile-management.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ApiError} from '../settings/api-error.model';
import {MessageBoxService} from '../settings/message-box.service';
import {PasswordResetComponent} from './password-reset/password-reset.component';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';






@Component({
  selector: 'app-auth-page',
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit {
  authType: String = '';
  title: String = '';
  errors: Errors = {errors: {}};
  isSubmitting = false;
  authForm: FormGroup;
  public login: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    private profileManagementService: ProfileManagementService,
    private snackBarService: MatSnackBar,
    private messageBoxService: MessageBoxService,
    private dialog: MatDialog
  ) {

    // use FormBuilder to create a form group
    this.authForm = this.fb.group({
      'emailId': ['', Validators.required],
      'password': ['', Validators.required]
    });
  }

  ngOnInit() {
    this.route.url.subscribe(data => {
      this.authType = data[data.length - 1].path;
      this.title = (this.authType === 'login') ? 'Sign in' : 'Sign up';

      if (this.authType === 'register') {
        this.authForm.addControl('name', new FormControl());
        this.authForm.addControl('type', new FormControl());
        this.authForm.controls['type'].setValue('user');

      }

    });
  }
  submitForm() {
  this.isSubmitting = true;
  this.errors = {errors: {}};
  const credentials = this.authForm.value;
  if (this.authType === 'login') {
    this.userService
      .attemptAuth(this.authType, credentials)
      .subscribe(
        data => this.router.navigateByUrl('/home'),

        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      );
  } else {
    this.profileManagementService.addOrUpdateProfile(this.authForm.getRawValue()).subscribe({

      next: (profile) => {
        this.snackBarService.open('Profile ' + profile.name + ' with Id ' + profile.profileId +
          ' added successfully. Please sign in to continue', 'Ok').afterDismissed().subscribe(() => {
          this.router.navigateByUrl('/login');
        });
      },
      error: (apiError: ApiError) => this.messageBoxService.info('Error: Failed to create a profile',
        apiError.title, apiError.detail)
    });
  }

  }

  otpTrigger() {
    const dialogRef = this.dialog.open( PasswordResetComponent);
  }
}
