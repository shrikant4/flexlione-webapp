import {  NgModule } from '@angular/core';
import { AuthComponent } from './auth.component';
import { NoAuthGuard } from './no-auth-guard.service';
import { SharedModule } from '../shared';
import { AuthRoutingModule } from './auth-routing.module';
import {DatePipe} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';

@NgModule({
  imports: [
    SharedModule,
    AuthRoutingModule
  ],
  declarations: [
    AuthComponent
  ],
  providers: [
    NoAuthGuard,
    DatePipe,
    MatSnackBar
  ]
})
export class AuthModule {}
