import { Component, OnInit } from '@angular/core';
import {FormBuilder,  FormGroup, Validators} from '@angular/forms';
import { Password} from './password.model';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})

export class PasswordResetComponent implements OnInit {
  resetPassword: FormGroup;
  constructor(private formBuilder: FormBuilder) {  }
  public otpSend = false;
  // Below code demonstrates how we can use validators in a form builder
  // Following is the link used for reference - https://blog.logrocket.com/angular-formbuilder-reactive-form-validation/
  // Additional link - https://coryrylan.com/blog/angular-form-builder-and-validation-management
  // We will be initialising form builder using Password model object

  // <--------Brief About validators------------------>
  // Validators.required -> Validator that requires the control have a non-empty value.
  //  validators.pattern -> we are using regex function to constraint atleast one special characters/length in our password/ atleast one capital alphabset / atleast one small alphabet
  // When you do fb.group({...}, {validator: fn}), the first argument are the controls of the group and the second are configuration parameters
  // for the group object itself, not the controls contained in it. Please check - https://stackoverflow.com/questions/49346428/angular-formbuilder-group-not-accepting-array-of-validators
  // The ValidatorFn that you can pass there will be applied to the FormGroup object, so you could create custom functions to check for conditions on multiple controls in the group.
  // Valid keys for the `extra` parameter map are `validator` and `asyncValidator`.
  ngOnInit(): void {
    this.resetPassword = this.formBuilder.group (new Password(), {validator: this.ConfirmedValidator('newPassword', 'confirmPassword')} );
    this.resetPassword.controls['emailId'].setValidators([Validators.required, Validators.email]);
    this.resetPassword.controls['newPassword'].setValidators([Validators.required, Validators.pattern(
         /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_-]).{8,}/), ]);
    this.resetPassword.controls['confirmPassword'].setValidators([Validators.required, Validators.pattern(
      /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_-]).{8,}/), ]);
    this.resetPassword.controls['otp'].setValidators([Validators.required]);
      }

  // here we are checking if  any errors already generated by failing validation -> in that case do nothing
  // If no error -> then also check for values in password and new password
  ConfirmedValidator(controlName: string, matchingControlName: string) {
    return (resetPassword: FormGroup) => {
      const control = resetPassword.controls[controlName];
      const matchingControl = resetPassword.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.confirmedValidator
    ) {
      return;
    }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }
  sendOTP() {
this.otpSend = true;
console.log(this.resetPassword.controls['emailId'].value);

  }

  resetPass() {
this.otpSend = false;
    console.log(this.resetPassword.value);
    const finalData = {};
   // Object.assign(this.finalData, this.resetPassword.value);
  }
}
