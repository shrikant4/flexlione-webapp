import {Validators} from '@angular/forms';

export class Password {
  emailId = ['', Validators.required];
  newPassword = '';
  confirmPassword = '';
  otp = '';
}
