import { NgModule } from '@angular/core';
import { EditorComponent } from './editor.component';
import { EditableArticleResolver } from './editable-article-resolver.service';
import { SharedModule } from '../shared';
import { EditorRoutingModule } from './editor-routing.module';
import {ArticleModule} from '../article/article.module';
import {MatButtonModule} from '@angular/material/button';
import {MatChip} from '@angular/material/chips';

@NgModule({
    imports: [
      SharedModule,
      EditorRoutingModule,
      ArticleModule,
      MatButtonModule
    ],
  declarations: [EditorComponent],
  providers: [EditableArticleResolver,
  MatChip]
})
export class EditorModule {}
