import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
// @ts-ignore
import {Article, ArticlesService, UserService} from '../core';
import {Store} from '@ngrx/store';
import {AppState} from '../app.state';
import {TaskModel} from '../shared/task-form/task.model';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ProfileModel} from '../profile/profile-models/profile.model';
import {maxDate} from '@rxweb/reactive-form-validators';


@Component({
  selector: 'app-editor-page',
  templateUrl: './editor.component.html'
})
export class EditorComponent implements OnInit {
  article: Article = {} as Article;
  errors: Object = {};
  isSubmitting = false;
  public loggedInUserId: string;
  currentUser: ProfileModel;
  EditTaskForm: FormGroup;


  constructor(
    private articlesService: ArticlesService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
  }




  ngOnInit() {

    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
        this.loggedInUserId = this.currentUser.profileId;
      }
    );
    this.EditTaskForm = this.formBuilder.group(new TaskModel());
    this.EditTaskForm.patchValue({
      status: 'yettostart',
      parentTaskId: '2',

      deadline: new Date(),
      positionAfter: '' ? null : this.EditTaskForm.getRawValue().positionAfter

    });
    this.EditTaskForm.controls['assignedTo'].setValue(this.loggedInUserId);
    this.EditTaskForm.controls['createdBy'].setValue(this.loggedInUserId);
    this.EditTaskForm.controls['hrsSpentTillNow'].disable();


    // Creating a Form EditTaskForm of Class Formgroup and passing it down to Task-Form Component
    this.store.select('createTask').subscribe({
      next: (x) => {
        if (x === undefined) {
          return;
        }
        if (x[0] !== undefined) {

          this.EditTaskForm.controls['taskId'].disable();
          this.EditTaskForm.controls['parentTaskId'].setValue(x[0].parentTaskId);
          // this.EditTaskForm.controls['taskId'].setValue('server generated');
        }
      }
    });
    this.changeDetectorRef.detectChanges();
  }
  onAddFromTemplate(): void {
    this.router.navigateByUrl('/template');
  }
}
