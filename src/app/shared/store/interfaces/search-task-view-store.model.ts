

export interface SearchTaskViewStoreModel {
  taskId: string;
  parentTaskId: string;
  createdBy: string;
  assignedTo: string;
  status: string;
  description: string;
  deadline: Date;
  isRemoved: boolean;
  createdAt: string;
editedAt: Date;
  label: string[];
}

