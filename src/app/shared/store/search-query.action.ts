// Section 1
import { Action } from '@ngrx/store';
import {SearchQuery} from '../search-filter/search-filter.model';





// Section 2
// We're defining the type of action, which is in the form of a string constant.
export const ADD_SEARCH_QUERY      = '[SearchQuery] Add';
export const REMOVE_SEARCH_QUERY    = '[SearchQuery] Remove';


// Section 3
// We're creating a class for each action with a constructor that allows us to pass in the payload
// We are also defining Type of action for this particular class

export class AddSearchQuery implements Action {
  readonly type = ADD_SEARCH_QUERY;

  constructor(public payload: SearchQuery) {}
}


export class RemoveSearchQuery implements Action {
  readonly type = REMOVE_SEARCH_QUERY;

  constructor() {}
}


// Section 4
export type SearchQueryActions = AddSearchQuery | RemoveSearchQuery;

