
import * as SearchQueryAction from './search-query.action';
import {SearchQuery} from '../search-filter/search-filter.model';



export function SearchQueryReducer(state: SearchQuery[] = [], action: SearchQueryAction.SearchQueryActions) {

  // Section 3
  // First, we use a switch to determine the type of action.
  // In the case of adding a tutorial, we return the new state

  // What have we done in the return statement?
  // ...state,X is a method to push X into array state
  switch ( action.type) {
    case SearchQueryAction.ADD_SEARCH_QUERY:
      const  newSearchQuery = [];
      newSearchQuery.push(action.payload);
      return newSearchQuery;
      case SearchQueryAction.REMOVE_SEARCH_QUERY:
          const emptySearchQuery = [];
          return emptySearchQuery;
          default:
            return state;
  }
}


