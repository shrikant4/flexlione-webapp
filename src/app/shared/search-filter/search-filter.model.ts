
export class  SearchQuery {
  Tag = null;
  deadline?: string;
  createdBy?: string[];
  assignedTo?: string[];
  description?: string;
  status?: string[];
  includeRemoved?: boolean;
  taskId: string ;
  createdAt?: string;
}

