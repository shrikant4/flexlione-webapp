import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import { FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {SearchQuery} from './search-filter.model';
import {SearchManagementService} from '../../Services/search-management.service';
import {ApiError} from '../../settings/api-error.model';
import {MessageBoxService} from '../../settings/message-box.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import * as TaskActions from '../store/search-task.action';
import {DatePipe} from '@angular/common';
import {getStatusList} from '../shared-lists/status-list';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute} from '@angular/router';
import {ProfileManagementService} from '../../Services/profile-management.service';
import {TaskManagementService} from '../../Services/task-management-service';
import {ProfileStoreModel} from '../store/interfaces/profile-store.model';
import {PageEvent} from '@angular/material/paginator';



/** @title Form field appearance variants */
@Component({
  selector: 'app-home-search-form',
  templateUrl: './search-filter.component.html',
})
export class SearchFilterComponent implements  OnInit {

 //  FilteredOptions is Observable array created for Auto filling

   @Input()
  searchForm: FormGroup; // this is parent form
  public  options: string[] = [];
  filteredOptions: Observable<string[]>;
  public AddTag = '';
  UserList: string[] = [];
  StatusList: string[] = getStatusList();
  initialValue = true;
  public clicked = false;
  public Profiles: ProfileStoreModel[] = [];
public pageSize = 5;
public pageIndex = 1;
  public length = 20;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  constructor(
    private searchManagementService: SearchManagementService,
    private  messageBoxService: MessageBoxService,
    private store: Store<AppState>,
    private datePipe: DatePipe,
    private  snackBarService: MatSnackBar,
    private route: ActivatedRoute,
    private  profileManagementService: ProfileManagementService,
    private taskManagementService: TaskManagementService) {


  }


  async getTaskSearchList(Search: SearchQuery, pageIndex, pageSize) {
    this.clicked = true;

    this.searchManagementService.getTaskSearchList(Search, pageIndex, pageSize)
      .subscribe({
        next: (taskList) => {
          this.clicked = false;
          this.store.dispatch(new TaskActions.RemoveSearchTask());
          this.store.dispatch(new TaskActions.AddSearchTask(taskList));
          },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: TaskModel not updated .',
            apiError.title, apiError.detail);
        }
      });
  }
   onSearch() {

    // const search = this.searchForm.getRawValue();
    // search.createdBy =  await this.GetProfileId(this.searchForm.getRawValue().createdBy);
    // search.assignedTo =  await this.GetProfileId(this.searchForm.getRawValue().assignedTo);
    // Get search-filter result for search query
   this.getTaskSearchList(this.createSearch(this.searchForm), this.pageIndex, this.pageSize);

  }


  public _searchByTag(tag: string) {
    const searchQuery: SearchQuery = new SearchQuery();
    searchQuery.Tag = tag;
    if (tag === undefined) {
      return;
    }
    this.getTaskSearchList(searchQuery, this.pageIndex, this.pageSize);
  }
  private _filter(value: string): string[] {
    return this.options.filter(option => option.toLowerCase().includes(
      value === undefined ? '' : value.toLowerCase()
    ));
  }
  //


  private  createSearch(newSearch: FormGroup) {
    const search = new SearchQuery();
    if (newSearch.getRawValue().description !== '' && newSearch.getRawValue().description !== undefined)  {
      search.description = newSearch.getRawValue().description;
    }
    if (newSearch.getRawValue().createdBy !== '' && newSearch.getRawValue().createdBy !== undefined)  {

      search.createdBy = [ this.GetProfileId(newSearch.getRawValue().createdBy)];
    }

    if (newSearch.getRawValue().assignedTo !== '' && newSearch.getRawValue().assignedTo !== undefined) {
      search.assignedTo = [ this.GetProfileId(newSearch.getRawValue().assignedTo)];

    }
    if (newSearch.getRawValue().deadline !== '' && newSearch.getRawValue().deadline !== undefined) {
      search.deadline = this.datePipe.transform(newSearch.getRawValue().deadline, 'yyyy-MM-dd');
    }

    if (newSearch.getRawValue().status !== '' && newSearch.getRawValue().status !== undefined) {
      search.status = [newSearch.getRawValue().status];
    }

    if (newSearch.getRawValue().includeRemoved !== '' && newSearch.getRawValue().includeRemoved !== undefined) {
      search.includeRemoved = newSearch.getRawValue().includeRemoved;
    }

    if (newSearch.getRawValue().taskId !== '' && newSearch.getRawValue().taskId !== undefined) {
      search.taskId = newSearch.getRawValue().taskId;
    }
    if (newSearch.getRawValue().createdAt !== '' && newSearch.getRawValue().createdAt !== undefined) {
      search.createdAt = this.datePipe.transform(newSearch.getRawValue().createdAt, 'yyyy-MM-dd');
    }
    return search;
  }

  public onAdd() {
    console.log(this.AddTag);
    this.searchManagementService.addTag(this.AddTag, 'SearchTag').subscribe({
      next: (tag) => {
        console.log(tag);
        this.snackBarService.open('Tag: ' + this.AddTag + ' has been successfully added',
          '', { duration: 3000 });
        this.AddTag = '';
      },
      error: (apiError: ApiError) => {this.messageBoxService.info('Error in adding Tag',
        apiError.title, apiError.detail); }
    });
  }

  ngOnInit() {

    this.filteredOptions = this.searchForm.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value.tag)),
    );
    this.profileManagementService.getAllProfiles().subscribe(
      profs => {
                for (let i = 0; i < profs.length; i++ ) {
          this.UserList.push(profs[i].name);
        }
      }
    );
  }

  public  GetProfileId(profileName: string) {
    this.profileManagementService.getAllProfiles().subscribe(
      profiles => {this.Profiles = profiles; }
    );
    const profile = this.Profiles.filter(function (value) {
      return (value.name === profileName);
    });
    return profile[0] === undefined ? profileName : profile[0].profileId;

  }

  updateList($event: PageEvent) {
    this.pageIndex =  $event.pageIndex + 1;
    this.pageSize = $event.pageSize;
    this.onSearch();
  }
}
