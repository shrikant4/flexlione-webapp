import {Component, Inject, OnInit} from '@angular/core';
import {TaskManagementService} from '../../../Services/task-management-service';
import {ChecklistManagementService} from '../../../Services/checklist-management.service';
import {MessageBoxService} from '../../../settings/message-box.service';
import {getResultList} from '../../shared-lists/result-list';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CheckListItem} from '../check-list-item.model';
import {ApiError} from '../../../settings/api-error.model';
import {UserService} from '../../../Services/user.service';
import {ProfileModel} from '../../../profile/profile-models/profile.model';



@Component({
  selector: 'app-add-edit-checklist',
  templateUrl: './add-edit-checklist.component.html',
  styleUrls: ['./add-edit-checklist.component.css']
})
export class AddEditChecklistComponent implements OnInit {


  private taskManagementService: TaskManagementService;
  private checklistManagementService: ChecklistManagementService;
  private messageBoxService: MessageBoxService;
  public isEdit: boolean ;
  public taskId: string;
  public StatusList: string[] = ['Completed', 'NotCompleted'];
  public selectedStatus = 'NotCompleted';
  public essentialList: boolean [] = [true, false];
  ResultList: string[] = getResultList(); // Hard coded
  public resultSelected: string;
  public result: string;
  public typeId: string;
  public checkListType: string;
  public data: any;
  currentUser: ProfileModel;
  // members for data-binding
  public clicked = false;

  newChecklistItem: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddEditChecklistComponent>,
              taskManagementService: TaskManagementService,
              messageBoxService: MessageBoxService,
              @Inject(MAT_DIALOG_DATA) data,
              checklistManagementService: ChecklistManagementService,
              private formBuilder: FormBuilder,
              private userService: UserService) {

    this.messageBoxService = messageBoxService;
    this.taskManagementService = taskManagementService;
    this.isEdit = data.isEdit;
    this.typeId = data.typeId;
    this.checkListType = data.type;
    this.checklistManagementService = checklistManagementService;
    this.data = data;

  }
  ngOnInit() {
    this.dialogRef.updateSize('50%', '80%');

    if (this.isEdit) {
      console.log(this.data.checkListItem);
      this.newChecklistItem = this.formBuilder.group(this.data.checkListItem);
      // this.newChecklistItem.addControl('assignmentType', new FormControl());
      // this.newChecklistItem.controls['assignmentType'].setValue(this.data.checkListItem.assignmentType);
      // this.newChecklistItem.addControl('managerComment', new FormControl());
      // this.newChecklistItem.controls['managerComment'].setValue('');
      console.log(this.newChecklistItem.value);
    } else {
      this.newChecklistItem = this.formBuilder.group(new CheckListItem());
      this.newChecklistItem.patchValue({
        typeId: this.typeId,
        checkListType: this.checkListType,
        status: this.selectedStatus
      });
      // this.newChecklistItem.addControl('assignmentType', new FormControl());
      // this.newChecklistItem.controls['assignmentType'].setValue(this.data.type);
    }
    this.newChecklistItem.controls['checkListItemId'].disable();
      this.newChecklistItem.controls['typeId'].disable();


    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
              });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  onCreate(): void {
    console.log(this.newChecklistItem.getRawValue);
    this.checklistManagementService.createOrUpdateCheckListItem( this.newChecklistItem.getRawValue(), this.currentUser.profileId)
      .subscribe({
      next: (checkListItem: CheckListItem) => {
        console.log('New Checklist successfully created');
        console.log(checkListItem);
        this.dialogRef.close(checkListItem);
        this.clicked = true;
      },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Could not create Checklist .', apiError.title, apiError.detail);
        }
    });
  }

  onUpdate(): void {
    const editedCheckList = this.newChecklistItem.getRawValue();
    // editedCheckList.managerComment = '';
    this.checklistManagementService.createOrUpdateCheckListItem(editedCheckList, this.currentUser.profileId)
      .subscribe({

        next: (checkListItem: CheckListItem) => {
          // this.newChecklistItem.addControl('assignmentType', new FormControl());
          this.clicked = true;
          console.log('TaskModel successfully updated');
          console.log(checkListItem);
          this.dialogRef.close(checkListItem);
        },
        error: (apiError: ApiError) => {this.messageBoxService.info('Error in updating Checklist',
          apiError.title, apiError.detail); }
      });
  }


  selectResult(value) {
    this.resultSelected  = value ;

  }

  }
