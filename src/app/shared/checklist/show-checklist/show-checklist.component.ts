import {Component, Inject, Input, OnInit} from '@angular/core';
import {TaskModel} from '../../task-form/task.model';
import {Template} from '../../../template-tasks/template.model';
import {CheckListItem} from '../check-list-item.model';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ChecklistManagementService} from '../../../Services/checklist-management.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AddEditChecklistComponent} from '../add-edit-checklist/add-edit-checklist.component';
import {MessageBoxService} from '../../../settings/message-box.service';
import {ApiError} from '../../../settings/api-error.model';
import {UserService} from '../../../Services/user.service';
import {ProfileModel} from '../../../profile/profile-models/profile.model';








@Component({
  selector: 'app-show-checklist',
  templateUrl: './show-checklist.component.html',
  styleUrls: ['./show-checklist.component.css']
})
export class ShowChecklistComponent implements OnInit {
  task: TaskModel;
  taskId: string;
  template: Template;
  templateId: string;
  checkListItemList: CheckListItem[] = [];
  selectedCheckListItem = '';
  public BooleanResultList: boolean[] = [true, false];
  public StatusList: string[] = ['NotCompleted', 'Completed'];
  public userComment = '';

  public fileLink = '';

  editForm: FormGroup;
  public checkListType: string;
  public isEdit: boolean;
  currentUser: ProfileModel;

  @Input() id = '';
  @Input() type = '';
  constructor(
    private route: ActivatedRoute,
    private checklistManagementService: ChecklistManagementService,
    private dialog: MatDialog,
    private snackBarService: MatSnackBar,
    private dialogRef: MatDialogRef<AddEditChecklistComponent, ShowChecklistComponent>,
    private messageBoxService: MessageBoxService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService

  ) {
    // this.editForm = this.editedForm();
    this.checkListType = data.checkListType;

    this.isEdit = data.isEdit;
    this.editForm = this.formBuilder.group(new CheckListItem());

  }

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      });

    if (this.type === 'task') {
      this.route.data.subscribe(
          (data: { article: TaskModel }) => {
            this.task = data.article;
            this.taskId = data.article.taskId;
          });
    } else {
    this.id = this.data.typeId;
    this.type = this.data.checkListType;
    this.taskId = this.id;

      }
    this.loadCheckList();

  }

  loadCheckList() {
      this.checklistManagementService.getCheckList(this.id, this.type).subscribe(
        {
          // the response is already deserialized, and it is in the form of an array or object
          next: (checkListItems: CheckListItem[]) => {
            console.log(checkListItems);
            this.checkListItemList = checkListItems;
            this.checkListItemList.forEach(element => element['isEdit'] = false);
          },
          error: (apiError: ApiError) => {this.messageBoxService.info('Error in loading Checklist',
            apiError.title, apiError.detail); }
        });
  }

  // OnInit() {
  //   this.dialogRef.updateSize('100%', '90%');
  //   this.id = this.data.typeId;
  //   this.type = this.data.checkListType;
  // }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onCreateNewCheckListItemClick(): void {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
      dialogConfig.data = {
        isEdit: false,
        typeId: this.id,
        type: this.type,
      };
    this.dialog.open(AddEditChecklistComponent, dialogConfig)
      .afterClosed().subscribe(
      {
        next: (checkListItem: CheckListItem) => {
          if (checkListItem == null) { // Cancel button clicked
            return;
 }
          this.loadCheckList();
          this.snackBarService.open('Success. New Checklist has been  created.', '', {duration: 3000});
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Could not create New Checklist.', apiError.title, apiError.detail);

        }, }
    );
  }

  onEditCheckListItemClick(checkListItemId: string): void {

    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    if (this.taskId) {
      dialogConfig.data = {
        isEdit: true,
        typeId: this.taskId,
        checkListItem: this.checkListItemList.filter(function (checklistItem) {
          return checklistItem.checkListItemId === checkListItemId;
        })[0]
      };
    } else {
      dialogConfig.data = {
        isEdit: true,
        typeId: this.template.templateId,
        checkListItem: this.checkListItemList.filter(function (checklistItem) {
          return checklistItem.checkListItemId === checkListItemId;
        })[0]
      };
    }

    this.dialog.open(AddEditChecklistComponent, dialogConfig)
      .afterClosed().subscribe(
      {
        next: (checkListItem: CheckListItem) => {
          if (checkListItem == null) { // Cancel button clicked
            return;
          }
          this.loadCheckList();
          this.snackBarService.open('Success. Checklist has been updated successfully.', '', {duration: 3000});
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Could not update Checklist.',
            apiError.title, apiError.detail);
        }
      }
    );
  }


  onDeleteCheckListItemClick(checkListItemId: string): void {

    // Open a dialog box to ask for confirmation
    this.messageBoxService.confirmWarn(
      'Are you sure you want to delete checkList Item ' + checkListItemId + '?', 'Delete')
      .afterClosed().subscribe({

      next: (proceed: boolean) => {

        // if proceed is true, that means user has confirmed
        if (proceed) {

          // send request to server to delete the PTL station
          this.checklistManagementService.deleteCheckListItem(checkListItemId).subscribe({
            next: () => {
              this.loadCheckList();
              // show acknowledgement to user
              this.snackBarService.open('Checklist item deleted.');

            },

            // show error dialog box if server failed to delete
            error: (apiError: ApiError) => {
              this.messageBoxService.info('Error: Failed to delete Checklist item.',
                apiError.title, apiError.detail);
            }
          });
        }
      }
    });


  }
  onRowClick(url: string): void {
    window.open(url, '_blank'); // open link in new tab

  }

    onSave(checkListItem) {
    checkListItem.isEdit = false;
    checkListItem.result = this.editForm.getRawValue().result;
    checkListItem.userComment = this.editForm.getRawValue().userComment;
    checkListItem.status = this.editForm.getRawValue().status === '' ? checkListItem.status : this.editForm.getRawValue().status;
    checkListItem.description = this.editForm.getRawValue().description;
    this.checklistManagementService.createOrUpdateCheckListItem(checkListItem, this.currentUser.profileId)
      .subscribe({
        next: (editConfirmCheckListItem) => {
          console.log('New Checklist successfully created');
          console.log(editConfirmCheckListItem);
          this.snackBarService.open('Checklist item updated.', '', {duration: 3000});
          this.loadCheckList();
          this.editForm.reset();

        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Failed to update Checklist item.', apiError.title, apiError.detail);
        }
      });
  }


  onEdit(checkListItem) {

    const setValue = {
      status: checkListItem.status,
      result: checkListItem.result,
      userComment: checkListItem.userComment,
      description: checkListItem.description,

    };
    this.editForm.patchValue(setValue);
    checkListItem.isEdit = true;
  }

}
