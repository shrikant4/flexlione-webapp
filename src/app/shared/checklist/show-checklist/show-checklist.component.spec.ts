import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowChecklistComponent } from './show-checklist.component';

describe('ShowChecklistComponent', () => {
  let component: ShowChecklistComponent;
  let fixture: ComponentFixture<ShowChecklistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowChecklistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
