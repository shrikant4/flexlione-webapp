


export class CheckListItem {
  checkListItemId  = 'server-generated';

  typeId: string = null;

  description = '';

  status = '';

  worstCase ? = 0;

  bestCase ? = 0;

  resultType = '';

  result ? = '';

  userComment ? = '';

  managerComment?: string;

  essential = true;

  isEdit?: boolean;

  checkListType: string;
}
