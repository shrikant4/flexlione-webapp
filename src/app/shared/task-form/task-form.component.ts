  import {AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {MessageBoxService} from '../../settings/message-box.service';
import {TaskManagementService} from '../../Services/task-management-service';
import {ApiError} from '../../settings/api-error.model';
import {getStatusList} from '../shared-lists/status-list';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {ProfileManagementService} from '../../Services/profile-management.service';
import {ProfileStoreModel} from '../store/interfaces/profile-store.model';
import {ProfileModel} from '../../profile/profile-models/profile.model';
import {UserService} from '../../Services/user.service';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['../../article/article.component.css']
})
export class TaskFormComponent implements OnInit, AfterViewInit {
  UserList: string[] = []; // get From Api
  Users: ProfileStoreModel[] = [];
  StatusList: string[] = getStatusList(); // Hard coded
  Profiles: ProfileStoreModel[] = [];
  currentUser: ProfileModel;
  public taskIdListForPosition: {'taskId': string, 'description': string}[];
  public taskIdListForParent: {'taskId': string, 'description': string}[];
  public loggedInUserId: string;
  @Input() taskForm;
  public clicked = false;
  constructor(
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private messageBoxService: MessageBoxService,
    private taskManagementService: TaskManagementService,
    private snackBarService: MatSnackBar,
    private  router: Router,
    private store: Store<AppState>,
    private profileManagementService: ProfileManagementService,
    private userService: UserService,
      ) {
  }


  ngAfterViewInit() {
      this.taskManagementService.getTaskIdList('', this.getTaskIdListForParent);
    this.taskManagementService.getTaskIdList(this.taskForm.getRawValue().parentTaskId, this.getTaskIdListForPosition);
  }




  getTaskIdListForParent = (taskIdList: {'taskId': string, 'description': string}[]) => { // Assigning parent task id from all existing task
    this.taskIdListForParent = taskIdList;
  }
  getTaskIdListForPosition = (taskIdList: {'taskId': string, 'description': string}[]) => { // Assigning position after task id from all sibling tasks
    this.taskIdListForPosition = taskIdList;
  }

 ngOnInit() {
   this.profileManagementService.getAllProfiles().subscribe({
     next : (prof) => {
       for (let i = 0; i < prof.length; i++) {
         this.Users.push(prof[i]); }
     },
     error: (apiError: ApiError) => {
       this.messageBoxService.info('Error!', apiError.title, apiError.detail);
     }
   });
   this.userService.currentUser.subscribe(
     (userData) => {
       this.currentUser = userData;
       this.loggedInUserId = this.currentUser.profileId;
     });
   this.taskManagementService.getTaskIdList('', this.getTaskIdListForParent);

   this.profileManagementService.getAllProfiles().toPromise().then((x) => this.Profiles = x);
   for (let i = 0; i < this.Profiles.length; i++ ) {
     this.UserList.push(this.Profiles[i].name);
   }

   this.taskForm.addControl('label', new FormControl());

   this.taskForm.controls['createdAt'].setValue(new Date());
   this.taskForm.controls['createdAt'].disable();
  }
  onClick() {
    const newTaskForm = this.taskForm.getRawValue();
    newTaskForm.deadline = this.datePipe.transform(this.taskForm.getRawValue().deadline, 'yyyy-MM-dd');
this.clicked = true;
    console.log(this.taskForm.getRawValue());
    this.taskManagementService.createOrUpdateTask(newTaskForm, this.loggedInUserId)
      .subscribe({
        next: (task) => {
          console.log(task);
          this.snackBarService.open('Success. Task has been updated.', '', {duration: 3000});
          this.router.navigateByUrl('/article/' + task.taskId);
          this.clicked = false;
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Task not updated .', apiError.title, apiError.detail);
          this.clicked = false;
        }
      });
  }
  onParentTaskIdChange(taskId: string) {
    this.taskManagementService.getTaskIdList(taskId, this.getTaskIdListForPosition);
    this.taskForm.controls['positionAfter'].setValue('');
  }
}
