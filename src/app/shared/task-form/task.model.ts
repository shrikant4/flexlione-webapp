
export class TaskModel {
  taskId = '';
  parentTaskId = '';
  createdAt = '';
  deadline = '';
  createdBy = '';
  assignedTo = '';
  status = '';
  sprintId: string = null;
  positionAfter = '';
  isRemoved = false;
  description = '';
  score = 0;
  expectedHours = 0;
  actualHours: number;
  children: TaskModel[] = [];
  hrsSpentTillNow = 0;
  acceptanceCriteria = 0;

  unplannedTaskData = {
    id: '',
    sprintId: '',
    taskId: '',
    scoreStatus: 'notRequested',
    profileId: '',
    requestedHours: 0,
    approvedHours: 0
  };
  label: string[];
  }

