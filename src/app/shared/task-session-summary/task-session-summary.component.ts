
import {TaskModel} from '../task-form/task.model';
import {Component, Input} from '@angular/core';
import {TaskSummaryModel} from '../../profile/daily-plan-summary/task-summary/task-summary.model';


@Component({
  selector: 'app-article-task-session-summary',
  templateUrl: './task-session-summary.component.html',
  styleUrls: ['../../article/article.component.css']
})

export class TaskSessionSummaryComponent {
  @Input() TaskSummaryList: TaskSummaryModel[];
  task: TaskModel;

  constructor() {
     }

}
