import {Component, Input} from '@angular/core';
import {  ArticleListConfig, ArticlesService } from '../../../core';
import {SearchTaskViewStoreModel} from '../../store/interfaces/search-task-view-store.model';
import { MessageBoxService } from '../../../settings/message-box.service';
import {TaskManagementService} from '../../../Services/task-management-service';
import {SearchManagementService} from '../../../Services/search-management.service';
import {Store} from '@ngrx/store';
import {PageEvent} from '@angular/material/paginator';
import {AppState} from '../../../app.state';
import {ProfileStoreModel} from '../../store/interfaces/profile-store.model';
import {ProfileManagementService} from '../../../Services/profile-management.service';

@Component({
  selector: 'app-article-list',
  styleUrls: ['article-list.component.css'],
  templateUrl: './article-list.component.html'
})
export class ArticleListComponent {

  // @Input()
  // set config(config: ArticleListConfig) {
  //   this.runQuery();
  // }
  // MatPaginator Inputs

  // length = 100;
  // pageSize = 10;
  // pageSizeOptions: number[] = [5, 10, 25, 100];

  // MatPaginator Output
  // pageEvent: PageEvent;

  @Input() limit: number;
  results: SearchTaskViewStoreModel[];
  loading = false;
  currentPage = 1;
  totalPages: Array<number> = [1];
  Profiles: ProfileStoreModel[] = [];
  currentDate = new Date().toISOString();


  constructor (
    private articlesService: ArticlesService,
    private taskManagementService: TaskManagementService,
    private messageBoxService: MessageBoxService,
    private searchManagementService: SearchManagementService,
    private store: Store<AppState>,
    private profileManagementService: ProfileManagementService
  ) {
     store.select('searchTaskView').subscribe(
      {
        next: (results) => {
          console.log(results);
          this.results = results;
          // this.results = results.sort((a, b) => {
          //   return this.getTime(new Date(a.deadline)) - this.getTime(new Date(b.deadline));
          // });
          },
        error: () => {}
      });
     this.GetProfiles();
  }

  private getTime(date?: Date) {
    return date != null ? date.getTime() : 0;
  }
   setPageTo(pageNumber) {
    this.currentPage = pageNumber;
     }

  private async GetProfiles() {
    this.Profiles = await this.profileManagementService.getAllProfiles().toPromise();
  }
}
