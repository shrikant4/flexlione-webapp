import {Component, EventEmitter, Input,  Output} from '@angular/core';
import {SearchTaskViewStoreModel} from '../../store/interfaces/search-task-view-store.model';
import {ProfileStoreModel} from '../../store/interfaces/profile-store.model';
import {ProfileManagementService} from '../../../Services/profile-management.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AddOrEditScheduleDialogComponent} from '../../../profile/schedule/add-or-edit-schedule-dialog.component';

import {TaskScheduleModel} from '../../../profile/schedule/task-schedule.model';
import {TaskHierarchyManagementService} from '../../../Services/task-hierarchy-management.service';
import {TaskHierarchyModel} from '../../../tasks-hierarchy/task-hierarchy.model';
import {TaskScheduleManagementService} from '../../../Services/task-schedule-management.service';
import {ApiError} from '../../../settings/api-error.model';
import {MessageBoxService} from '../../../settings/message-box.service';

@Component({
  selector: 'app-article-preview',
  templateUrl: './article-preview.component.html',
  styleUrls: ['../../../app.component.css']
})
export class ArticlePreviewComponent {
  constructor( private profileManagementService: ProfileManagementService,
               private dialog: MatDialog,
               private  taskHierarchyManagementService: TaskHierarchyManagementService,
               private taskScheduleManagementService: TaskScheduleManagementService,
               private messageBoxService: MessageBoxService) {}
  @Output() newScheduleEvent  = new EventEmitter<TaskScheduleModel>();
  @Input() SearchTask: SearchTaskViewStoreModel;
  @Input() Profiles: ProfileStoreModel[];
  // searchTaskDeadline: string;

 // public Profiles: ProfileStoreModel[] = [];
  public TaskHierarchy: TaskHierarchyModel;
  currentDate = new Date().toISOString();





  onUpdateOrScheduleNewTask(task): void {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      task : task
    };

    this.dialog.open(AddOrEditScheduleDialogComponent, dialogConfig)
      .afterClosed().subscribe({
      next: (taskSchedule: TaskScheduleModel) => {
        if (taskSchedule !== undefined) {
          this.taskScheduleManagementService.emitTaskSchedule(taskSchedule);
        }
      },
      error: (apiError: ApiError) => {
        this.messageBoxService.info('Error: Could not update or Schedule new Task.', apiError.title, apiError.detail);
      }
    });
  }


  getTaskHierarchy(): void {
    this.taskHierarchyManagementService.getTaskHierarchyByTaskId(this.SearchTask.taskId, '', this.onSuccess);
  }
  public onSuccess = (taskHierarchy: TaskHierarchyModel) => {
    this.TaskHierarchy = taskHierarchy;
    this.TaskHierarchy.childrenTaskIdList = this.TaskHierarchy.childrenTaskIdList.reverse();
  }
}
