import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {MatAutocompleteTrigger} from '@angular/material/autocomplete';


@Component({
  selector: 'app-auto-search',
  templateUrl: './auto-search.component.html',
})
export class AutoSearchComponent implements OnInit, AfterViewInit  {

@Input() options;
@Input() description;
@Input() baseUrl;
@Input() search;
@Output() newItemEvent  = new EventEmitter<string>();
@ViewChild(MatAutocompleteTrigger ) trigger: MatAutocompleteTrigger;
filteredOptions: Observable<string[]>;
myControl = new FormControl();
 constructor() {}

  ngAfterViewInit() {
    // Clear the input and emit when a selection is made
    this.trigger.autocomplete.optionSelected
      .subscribe(option => {
        this.myControl.setValue('');
      });
  }
 ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
 }

  private _filter(value: string): string[] {
    return this.options.filter(option => option.toLowerCase().includes(
      value === undefined ? '' : value.toLowerCase()
    ));
  }

  onRowClick(selected: string) {
    this.newItemEvent.emit(selected);
  }


}

