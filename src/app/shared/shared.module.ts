import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ArticleListComponent, ArticleMetaComponent, ArticlePreviewComponent } from './article-helpers';
import { ListErrorsComponent } from './list-errors.component';
import { ShowAuthedDirective } from './show-authed.directive';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonModule} from '@angular/material/button';
import {AutoSearchComponent} from './auto-search/auto-search.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {SearchFilterComponent} from './search-filter/search-filter.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { TaskSessionSummaryComponent } from './task-session-summary/task-session-summary.component';
import { ShowChecklistComponent } from './checklist/show-checklist/show-checklist.component';
import { AddEditChecklistComponent } from './checklist/add-edit-checklist/add-edit-checklist.component';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import {RxReactiveFormsModule} from '@rxweb/reactive-form-validators';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatChipInput} from '@angular/material/chips';



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
        MatExpansionModule,
        MatButtonModule,
        MatAutocompleteModule,
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatIconModule,
        MatMenuModule,
        MatDialogModule,
        RxReactiveFormsModule,
        MatPaginatorModule,

    ],
  declarations: [
    ArticleListComponent,
    ArticleMetaComponent,
    ArticlePreviewComponent,
    ListErrorsComponent,
    ShowAuthedDirective,
    AutoSearchComponent,
    SearchFilterComponent,
    TaskSessionSummaryComponent,
    ShowChecklistComponent,
    AddEditChecklistComponent,

  ],
    exports: [
        ArticleListComponent,
        ArticleMetaComponent,
        ArticlePreviewComponent,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        ListErrorsComponent,
        RouterModule,
        ShowAuthedDirective,
        AutoSearchComponent,
        SearchFilterComponent,
        TaskSessionSummaryComponent,
        ShowChecklistComponent,

    ]
})
export class SharedModule {}
