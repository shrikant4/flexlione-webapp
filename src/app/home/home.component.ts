import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {ArticleListConfig, TagsService, User, UserService} from '../core';
import {SearchManagementService} from '../Services/search-management.service';
import {Store} from '@ngrx/store';
import {AppState} from '../app.state';
import {ApiError} from '../settings/api-error.model';
import {SearchQuery} from '../shared/search-filter/search-filter.model';
import * as TaskActions from '../shared/store/search-task.action';
import {MessageBoxService} from '../settings/message-box.service';
import {FormBuilder} from '@angular/forms';
import {TaskManagementService} from '../Services/task-management-service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SearchTaskViewStoreModel} from '../shared/store/interfaces/search-task-view-store.model';


@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', './../app.component.css']

})
export class HomeComponent implements OnInit {

  length = 100;
  pageSizeOptions: number[] = [5, 10, 20, 100];
  pageSize = 10;
  pageIndex = 1;
  TaskIdList: {'taskId': string, 'description': string}[] = [];
  public show = false;
  currentUser: User;
  public toggleDetailSearch = false;
  query: SearchQuery[];
  public clicked = false;
  @ViewChild('paginator') paginator: MatPaginator;
  sortedResult: SearchTaskViewStoreModel[];



    constructor(
    private router: Router,
    private tagsService: TagsService,
    private userService: UserService,
    private  searchManagementService: SearchManagementService,
    private store: Store<AppState>,
    private  messageBoxService: MessageBoxService,
    private taskManagementService: TaskManagementService,
    public globalSearch: SearchQuery,
    public personalSearch: SearchQuery,
    private formBuilder: FormBuilder,
    private snackBarService: MatSnackBar,
  ) {
    }

  parentForm = this.formBuilder.group({
      tag: null,
      deadline: '',
      createdBy: '',
      assignedTo: [''],
      description: '',
      status: [''],
      includeRemoved: false,
      taskId: '',
      createdAt: '',
    }

  );
  listConfig: ArticleListConfig = {
    type: 'all',
    filters: {}
  };
  ngOnInit() {
    this.globalSearch = this.searchManagementService.getGlobalSearch();
    this.setListTo(this.searchManagementService.getGlobalSearch());
    this.userService.currentUser.subscribe(
      (userData) => {
        this.personalSearch = this.searchManagementService.getPersonalSearch(userData.profileId);
      }
    );


  }



  setListTo(query: SearchQuery) {
    this.searchManagementService.getTaskSearchList(query, this.pageIndex, this.pageSize)
      .subscribe({
        next: (taskList) => {
          this.store.dispatch(new TaskActions.RemoveSearchTask());
          this.store.dispatch(new TaskActions.AddSearchTask(taskList.sort((a, b) => ( new Date(a.editedAt).valueOf() - new Date().valueOf() ) - ( new Date(b.editedAt).valueOf() - new Date().valueOf()))));
                   },
          error: (apiError: ApiError) => {
            this.messageBoxService.info('Error: TaskModel not updated .', apiError.title, apiError.detail);
          }
        });
  }

  toggle() {
     this.toggleDetailSearch = !this.toggleDetailSearch;
     }

  searchTask(description: string, pageIndex: number) {
    this.clicked = true;
    this.pageIndex = pageIndex;
    this.searchManagementService.getSearchResultForTask(description == null ? '' : description, this.pageIndex, this.pageSize).subscribe({
       next: (search) => {
          this.clicked = false;
         this.store.dispatch(new TaskActions.RemoveSearchTask());
         this.store.dispatch(new TaskActions.AddSearchTask(search));
         // this.sortedResult = search.sort((a, b) =>
         //   new Date(b.editedAt).valueOf() - new Date(a.editedAt).valueOf());
       console.log(search);
       },
      error: () => {
        this.snackBarService.open('No  more search results available for search query', '', {duration: 3000});
        this.clicked = false;
      }
     });
   }
  updateList(pageEvent: PageEvent) {
    console.log();
    this.pageIndex = pageEvent.pageIndex + 1;
    this.pageSize = pageEvent.pageSize;

  //  if (this.toggleDetailSearch) {
    if (this.parentForm.getRawValue().description === '') {
      this.setListTo(this.globalSearch);
    } else {
      this.searchTask(this.parentForm.getRawValue().description, this.pageIndex + 1);

    }
  }


}
