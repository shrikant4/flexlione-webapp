import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { HomeAuthResolver } from './home-auth-resolver.service';
import { SharedModule } from '../shared';
import { HomeRoutingModule } from './home-routing.module';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {SearchManagementService} from '../Services/search-management.service';
import {DatePipe} from '@angular/common';
import {SearchQuery} from '../shared/search-filter/search-filter.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


@NgModule({
    imports: [
        SharedModule,
        HomeRoutingModule,
        MatAutocompleteModule,
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
    ],

  declarations: [
    HomeComponent
  ],
  exports: [
  ],
  providers: [
    HomeAuthResolver,
    SearchManagementService,
    DatePipe,
    SearchQuery,
    MatSnackBar
  ]
})
export class HomeModule {}
