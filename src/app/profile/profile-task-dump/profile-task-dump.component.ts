import {AfterViewInit, Component, Input, OnDestroy} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {SearchTaskViewStoreModel} from '../../shared/store/interfaces/search-task-view-store.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProfileComponent} from '../profile.component';
import {TaskManagementService} from '../../Services/task-management-service';
import {TaskModel} from '../../shared/task-form/task.model';
import {SearchQuery} from '../../shared/search-filter/search-filter.model';
import {SearchManagementService} from '../../Services/search-management.service';
import {takeWhile} from 'rxjs/operators';
import {SearchQueryRepositoryService} from '../../Services/search-query-repository.service';
import {MessageBoxService} from '../../settings/message-box.service';
import {ApiError} from '../../settings/api-error.model';
import * as TaskActions from '../../shared/store/search-task.action';
import {ArticleListConfig} from '../../shared/article-helpers/article-list/article-list-config.model';

@Component({
  selector: 'app-profile-task-dump',
  templateUrl: './profile-task-dump.component.html'
})
export class ProfileTaskDumpComponent implements  AfterViewInit, OnDestroy {
  sortedResult: SearchTaskViewStoreModel[];
  @Input() options: string[] ;


  taskId = '';
  isAlive = true;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private  snackBarService: MatSnackBar,
    private profileComponent: ProfileComponent,
    private taskManagementService: TaskManagementService,
    private searchManagementService: SearchManagementService,
    private searchQueryRepositoryService: SearchQueryRepositoryService,
    private messageBoxService: MessageBoxService,

  ) {
    store.select('searchTaskView').subscribe(
      {
        next: (taskList) => {
          this.sortedResult = taskList.sort((a, b) =>
                    new Date(b.createdAt).valueOf() - new Date(a.createdAt).valueOf());
          console.log(this.sortedResult);
        },
        error: () => {}
      });

  }

  ngAfterViewInit(): void {
    // this.searchQueryRepositoryService.searchQueryInStore$.pipe(takeWhile(value => this.isAlive)).subscribe((searchQuery) => {
    //   this.setTaskDump(searchQuery[0]);
    // } );
    // this.searchQueryRepositoryService.readSearchQueryInStore();




    // Subscribing to observable  and taking action  when ever there is taskSchedule added
    // this.searchQueryRepositoryService.searchQueryInStore$.pipe(takeWhile(value => this.isAlive)).subscribe((searchQuery) => {
    //   this.setTaskDump(searchQuery[0]);
    // } );
    //  this.searchQueryRepositoryService.readSearchQueryInStore();
    //
    // this.store.select('searchTaskView').subscribe(
    //   {
    //     next: (taskList) => {
    //       this.sortedResult = taskList;
    //     },
    //     error: () => {}
    //   });

  }



  public onRowClick(taskId: string) {
    this.taskId = taskId;
  }
  onSuccess = (task: TaskModel)  => {
    this.snackBarService.open('Task Successfully linked to sprint-planner', '', {duration: 300});
  }
  addTaskToSprint(sprintId: string) {
  this.taskManagementService.onLinkTaskToSprint(sprintId, this.taskId, this.onSuccess);
  }

  // setTaskDump(search: SearchQuery) {
  //   this.searchManagementService.getTaskSearchList(search, 1, 100).subscribe({
  //     next : (taskList) => {
  //       this.sortedResult = taskList.sort((a, b) =>
  //         new Date(b.createdAt).valueOf() - new Date(a.createdAt).valueOf()); },
  //     error: (apiError: ApiError) => {
  //       this.messageBoxService.info('Error.', apiError.title, apiError.detail);
  //     }
  //   });
  //
  //
  // }

  ngOnDestroy(): void {
    // Stop subscribing to above observable when component is destroyed. Avoid data leakage.
    this.isAlive = false;
  }

}
