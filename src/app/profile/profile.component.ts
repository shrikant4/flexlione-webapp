import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute,  Router} from '@angular/router';
import {User, UserService, Profile} from '../core';
import {MatAccordion} from '@angular/material/expansion';
import {MatDialog} from '@angular/material/dialog';
import {SprintModel} from '../sprint/sprint.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProfileManagementService} from '../Services/profile-management.service';
import {ProfileStoreModel} from '../shared/store/interfaces/profile-store.model';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {SearchQuery} from '../shared/search-filter/search-filter.model';
import {map, startWith, takeWhile, tap} from 'rxjs/operators';
import {SearchQueryRepositoryService} from '../Services/search-query-repository.service';
import {ApiError} from '../settings/api-error.model';
import {MessageBoxService} from '../settings/message-box.service';
import {PageEvent} from '@angular/material/paginator';
import {SearchManagementService} from '../Services/search-management.service';
import * as TaskActions from '../shared/store/search-task.action';
import {Store} from '@ngrx/store';
import {AppState} from '../app.state';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Observable} from 'rxjs';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {query} from '@angular/animations';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile.component.html',
  styleUrls: ['profile.component.css']
})

export class ProfileComponent implements OnInit, AfterViewInit, OnDestroy {
  options: string[] = [];
  togglePlanner = 'day';
  sprintList: SprintModel[] = [];
  public profileId = '';
  profileName = '';
  searchQuery: SearchQuery;
  isAlive = true;
  public Profiles: ProfileStoreModel[] = [];
  @ViewChild(MatAccordion) accordion: MatAccordion;
  private sprintData: SprintModel[];
  public toggleDetailSearch = false;
  public clicked =  false;
  labelForm: FormGroup;
  length = 100;
  pageSize = 10;
  pageIndex = 1;
  pageSizeOptions: number[] = [5, 10, 20, 100];
  removable = true;
  labelCtrl = new FormControl();
  chipListCtrl = new FormControl([]);
  filteredLabels: Observable<string[]>;
  allLabels: string[] = ['notCompleted' , 'sprint'];
  chipList: [];

  @ViewChild('labelInput') labelInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private dialog: MatDialog,
    private snackBarService: MatSnackBar,
    private router: Router,
    private profileManagementService: ProfileManagementService,
    private searchManagementService: SearchManagementService,
    private searchQueryRepositoryService: SearchQueryRepositoryService,
    private messageBoxService: MessageBoxService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
  ) {
    // this.labelCtrl.valueChanges.pipe(tap(value => console.log('labelCtrl: ', value))).subscribe();
    // this.chipListCtrl.valueChanges.pipe(tap(value => console.log('chipListCtrl: ', value))).
    // subscribe({
    //   next: (chip) => {
    //     this.chipList = chip;
    //   }
    // });


    this.filteredLabels = this.labelCtrl.valueChanges.pipe(
      startWith(null),
      map((label: string | null) => label ? this._filterLabels(label) : this.allLabels.slice()));


    this.labelForm = this.formBuilder.group({
      fruitCtrl: '',
      chipListCtrl: [],
    });
  }
  parentForm = this.formBuilder.group({

    tag: null,
    deadline: '',
    createdBy: [''],
    assignedTo: [''],
    description: '',
    status: [''],
    includeRemoved: false,
    taskId: '',
    createdAt: '',
    // label: ['']
  });

  profile: Profile;
  currentUser: User;

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.profileId = userData.profileId; // Update  profile id and name
        this.profileName = userData.name;
        this.updateSprintList(this.profileId);
        this.searchQuery = new SearchQuery();
        this.searchQuery.assignedTo = [this.profileId];
      }
    );
    // this.parentForm.controls['assignedTo'].setValue( // cannot edit Owner on search Task filter
    //   this.GetProfileName(this.profileId)
    // );

    this.setListTo();
  }

  ngAfterViewInit(): void {

    // [FIXME] We are using same code below at multiple places , we can use a call back function and make below function common in the service
    // [FIXMEreplyRahul] Done.
    this.profileManagementService.getAllProfiles()

      .subscribe({
        next: (profiles) => {
          this.Profiles = profiles;
          this.profileName = this.GetProfileName(this.profileId);
          for (let i = 0; i < this.Profiles.length; i++) {
            this.options.push(this.Profiles[i].name);
          }
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error!', apiError.title, apiError.detail);
        }
      });


  }

  ngOnDestroy() {
    // Stop subscribing to above observable when component is destroyed. Avoid data leakage.
    this.isAlive = false;
  }

  // [FIXME] Sprintlist should be updated directly in the Sprint Preview Component like Calender and Summary Component
  public updateProfile(profileName: string) {
    this.profileId = this.GetProfileId(profileName);
    this.profileName = this.GetProfileName(this.profileId);
    this.searchQuery.assignedTo = [this.profileId];
    this.searchQueryRepositoryService.updateSearchQueryInStore(this.searchQuery);
    this.updateSprintList(this.profileId);
    this.setListTo();
  }


  public updateSprintList(profileId: string) {
    this.profileManagementService.getProfileById(profileId, 'sprint')
      .subscribe({
        next: (profile) => {
          this.sprintData = profile.sprints;
          this.sprintList = this.sprintData.sort((a, b) => (new Date(b.fromDate).valueOf() - new Date(a.fromDate).valueOf()));
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Could not update Sprint List.', apiError.title, apiError.detail);
        }
      });
  }


  public getSprintIds(): string[] {
    const sprintIds: string[] = [];
    this.sprintList.forEach(function (value) {
      sprintIds.push(value.sprintId);
    });
    return sprintIds;
  }

  public onSearchTasks() {
    this.togglePlanner = 'search';
  }

  public onPlanSprint() {
    this.togglePlanner = 'sprint-planner';
    // Fetch Sprint list
    this.updateSprintList(this.profileId); // Asynchronously Update Sprint lIST
    this.searchQuery.description = 'sprint';
    this.searchQueryRepositoryService.updateSearchQueryInStore(this.searchQuery);
  }

  public onPlanDay() {
    this.togglePlanner = 'day';
    this.searchQuery = new SearchQuery();
    this.searchQuery.assignedTo = [this.profileId];
    this.searchQueryRepositoryService.updateSearchQueryInStore(this.searchQuery);
  }

  public onDaySummary() {
    this.togglePlanner = 'summary';
  }

  private GetProfileId(profileName: string): string {
    const profile = this.Profiles.filter(function (value) {
      return (value.name === profileName);
    });
    return profile[0] === undefined ? profileName : profile[0].profileId;
  }

  private GetProfileName(profileId: string): string {
    const profile = this.Profiles.filter(function (value) {
      return (value.profileId === profileId);
    });
    return profile[0] === undefined ? profileId : profile[0].name;
  }

  setListTo() {

    this.parentForm.controls['assignedTo'].disable();
    const searchQuery = new SearchQuery();
    searchQuery.assignedTo = [this.profileId];
    this.searchQueryRepositoryService.updateSearchQueryInStore(searchQuery);
    this.searchQueryRepositoryService.searchQueryInStore$.pipe(takeWhile(value => this.isAlive)).subscribe((receivedQuery) => {
      this.searchQuery = receivedQuery[0];
    });

    this.searchManagementService.getTaskSearchList(this.searchQuery, this.pageIndex, this.pageSize)
      .subscribe({
        next: (taskList) => {
                  this.store.dispatch(new TaskActions.RemoveSearchTask());
          this.store.dispatch(new TaskActions.AddSearchTask(taskList
            .sort((a, b) => ( new Date(b.editedAt).valueOf() - new Date().valueOf() ) - ( new Date(a.editedAt).valueOf() - new Date().valueOf()))));
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: TaskModel not updated .', apiError.title, apiError.detail);
        }
      });
  }

  searchTask(description: string, pageIndex: number) {
    this.clicked = true;
    this.pageIndex = pageIndex;
    this.searchManagementService.getSearchResultForTask(description == null ? '' : description, this.pageIndex, this.pageSize).subscribe({
      next: (search) => {
        setTimeout(() => { this.clicked = false; }, 2000);
        this.store.dispatch(new TaskActions.RemoveSearchTask());
        this.store.dispatch(new TaskActions.AddSearchTask(search));
        console.log(search);
      },
      error: () => {
        this.snackBarService.open('No  more search results available for search query', '', {duration: 3000});
        this.clicked = false;
      }
    });
  }

  updateList(pageEvent: PageEvent) {
    this.pageIndex = pageEvent.pageIndex + 1;
    this.pageSize = pageEvent.pageSize;

    if (this.parentForm.getRawValue().description === '') {
      this.setListTo();
    } else {
      this.searchTask(this.parentForm.getRawValue().description, this.pageIndex + 1);
    }

    // if (this.toggleDetailSearch) {
    //   this.searchTask(this.parentForm.getRawValue().description);
    // } else {
    //   this.setListTo();
    // }
  }

  toggleNew() {
    this.toggleDetailSearch = !this.toggleDetailSearch;
  }

  remove(fruit: string): void {
    const index = this.chipListCtrl.value.indexOf(fruit);

    if (index >= 0) {
      const val = [...this.chipListCtrl.value];
      val.splice(index, 1);
      this.chipListCtrl.setValue(val);
    }
  }

  onNewChipSelected(event: MatAutocompleteSelectedEvent): void {
    event.option.deselect();
    const val = [...this.chipListCtrl.value];
    val.push(event.option.viewValue);
    this.chipListCtrl.setValue(val);
    this.labelInput.nativeElement.value = '';
    this.labelCtrl.setValue(null);
  }

  private _filterLabels(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allLabels.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  }


  filterTask () {
    this.clicked = true;
    this.profileManagementService.filterbyTags(this.profileId, this.chipListCtrl.value, this.pageIndex, this.pageSize ).subscribe({
      next: (Filter) => {
        this.clicked = false;
        this.store.dispatch(new TaskActions.RemoveSearchTask());
        this.store.dispatch(new TaskActions.AddSearchTask(Filter));
      },
      error: (apiError: ApiError) => {
        this.messageBoxService.info('Error: Could not sent request',
          apiError.title, apiError.detail);
      }
    });

  }
}
