import {TaskSummaryModel} from '../daily-plan-summary/task-summary/task-summary.model';

export class TaskScheduleModel {
  taskScheduleId = '';

  taskId = '';

  description = '';

  date = '';

  startHour = 0;

  startMinute = 0;

  stopHour = 0;

  stopMinute = 0;

  owner = '';

  taskSummaryId = '';

  isPlanned = true;

  taskSummary: TaskSummaryModel;

}
