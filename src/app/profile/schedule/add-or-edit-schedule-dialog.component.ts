import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { FormGroup, FormBuilder} from '@angular/forms';
import { MessageBoxService } from '../../settings/message-box.service';
import {DatePipe} from '@angular/common';
import {TaskScheduleManagementService} from '../../Services/task-schedule-management.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TaskScheduleModel} from '../schedule/task-schedule.model';
import {TaskScheduleRepositoryService} from '../../Services/task-schedule-repository.service';
import {ApiError} from '../../settings/api-error.model';

@Component({
  selector: 'app-add-or-edit-schedule-dialog',
  templateUrl: './add-or-edit-schedule-dialog.component.html',
})
export class AddOrEditScheduleDialogComponent implements OnInit {
  public isEdit: boolean;
  public taskId: string;
  newTaskSchedule: FormGroup;
  public clicked = false;

  constructor(private datepipe: DatePipe,
              public dialogRef: MatDialogRef<AddOrEditScheduleDialogComponent>,
              private messageBoxService: MessageBoxService,
              @Inject(MAT_DIALOG_DATA) data,
              private taskScheduleManagementService: TaskScheduleManagementService,
              private snackBarService: MatSnackBar,
              private taskScheduleRepositoryService: TaskScheduleRepositoryService,
              private formBuilder: FormBuilder, ) {
    this.newTaskSchedule = this.formBuilder.group(new TaskScheduleModel());
    this.newTaskSchedule.patchValue({
      taskScheduleId: 'Server Generated',
      taskId: data.task.taskId,
      description: data.task.description,
      owner: data.task.assignedTo,
      date:  new Date(),
    });
    this.newTaskSchedule.controls['description'].disable();
    this.newTaskSchedule.controls['taskId'].disable();
  }

  ngOnInit(): void {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onUpdate(): void {
    const newTaskSchedule = this.newTaskSchedule.getRawValue();
    newTaskSchedule.date = this.datepipe.transform(this.newTaskSchedule.getRawValue().date, 'yyyy-MM-dd');
    this.taskScheduleManagementService.AddOrUpdateTaskSchedule(newTaskSchedule)
      .subscribe({
        next: (taskSchedule) => {
          this.snackBarService.open('Success! Task Schedule updated', '', {duration: 300});
          this.clicked = true;
          this.dialogRef.close(taskSchedule);
          this.taskScheduleRepositoryService.AddNewTaskSchedule(taskSchedule.taskScheduleId, this.newTaskSchedule.getRawValue().owner);
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Could not update Task Schedule.',
            apiError.title, apiError.detail);
        }
      });

  }
}

