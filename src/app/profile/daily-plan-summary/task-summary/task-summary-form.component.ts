import {Component, OnInit, Input} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DailyPlanSummaryService} from '../../../Services/daily-plan-summary.service';
import {ApiError} from '../../../settings/api-error.model';
import {MessageBoxService} from '../../../settings/message-box.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../../app.state';
import {DatePipe} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute} from '@angular/router';
import { TaskSummaryModel} from './task-summary.model';
import {TaskScheduleRepositoryService} from '../../../Services/task-schedule-repository.service';

@Component({
  selector: 'app-profile-task-summary-form',
  templateUrl: './task-summary-form.component.html',
  styleUrls: ['../../profile.component.css']
})
export class TaskSummaryFormComponent implements  OnInit {
  @Input()
  set config(taskSummary: TaskSummaryModel) {
    this.updateTaskSummaryForm(taskSummary);
  }
  @Input()
  set profileId(profileId: string) {
    this.ProfileId = profileId;
  }
  newTaskSummary: FormGroup;
  ProfileId: string;

  constructor(
    private searchManagementService: DailyPlanSummaryService,
    private messageBoxService: MessageBoxService,
    private store: Store<AppState>,
    private datePipe: DatePipe,
    private snackBarService: MatSnackBar,
    private route: ActivatedRoute,
    private  dailyPlanSummaryService: DailyPlanSummaryService,
    private taskScheduleRepositoryService: TaskScheduleRepositoryService,
    private formBuilder: FormBuilder,
    private datepipe: DatePipe,
  ) {

    this.newTaskSummary = this.formBuilder.group( new TaskSummaryModel());
    this.newTaskSummary.patchValue({
      date: new Date(),
    });

    this.newTaskSummary.controls['taskId'].disable();
    this.newTaskSummary.controls['taskSummaryId'].disable();
    this.newTaskSummary.controls['description'].disable();
    this.newTaskSummary.controls['expectedHour'].disable();
    this.newTaskSummary.controls['systemHours'].disable();
    console.log(this.newTaskSummary.value);
  }


  ngOnInit() {

  }
  updateTaskSummaryForm(taskSummary: TaskSummaryModel) {
    this.newTaskSummary.patchValue({
      taskScheduleId: taskSummary.taskScheduleId == null ? 'server-generated' : taskSummary.taskScheduleId,
      taskSummaryId: taskSummary.taskSummaryId == null ? 'server-generated' : taskSummary.taskSummaryId,
      taskId: taskSummary.taskId == null ? 'server-generated' : taskSummary.taskId,
      description: taskSummary.description == null ? 'server-generated' : taskSummary.description,
      expectedOutput: taskSummary.expectedOutput == null ? '' : taskSummary.expectedOutput,
      expectedHour: taskSummary.expectedHour == null ? 'server-generated' : taskSummary.expectedHour,
      systemHours: taskSummary.systemHours == null ?  0 :  taskSummary.systemHours.toFixed(2),
        // toFixed is used to round a number upto  desired decimal places

      actualOutput: taskSummary.actualOutput,
      // actualHour: taskSummary.actualHour,
      date: taskSummary.date == null ? '' : taskSummary.date
    });
  }

  // private createTaskSummary(newTaskSummary: FormGroup): TaskSummaryModel {
  //   const taskSummary = new TaskSummaryModel();
  //   taskSummary.taskSummaryId = newTaskSummary.getRawValue().taskSummaryId;
  //   taskSummary.taskScheduleId = newTaskSummary.getRawValue().taskScheduleId;
  //   taskSummary.taskId = newTaskSummary.getRawValue().taskId;
  //   taskSummary.description = newTaskSummary.getRawValue().description;
  //   taskSummary.expectedHour = newTaskSummary.getRawValue().expectedHours;
  //   taskSummary.date = newTaskSummary.getRawValue().date;
  //   if (newTaskSummary.getRawValue().actualHours !== '' && newTaskSummary.getRawValue().actualHours !== undefined)  {
  //    taskSummary.actualHour = +newTaskSummary.getRawValue().actualHours;
  //   }
  //   if (newTaskSummary.getRawValue().expectedOutput !== '' && newTaskSummary.getRawValue().expectedOutput !== undefined)  {
  //     taskSummary.expectedOutput = newTaskSummary.getRawValue().expectedOutput;
  //   }
  //   if (newTaskSummary.getRawValue().actualOutput !== '' && newTaskSummary.getRawValue().actualOutput !== undefined)  {
  //     taskSummary.actualOutput = newTaskSummary.getRawValue().actualOutput;
  //   }
  //   return taskSummary;
  // }

  public onAdd() {
    const newTaskSummary = this.newTaskSummary.getRawValue();
    newTaskSummary.date = this.datepipe.transform(this.newTaskSummary.getRawValue().date, 'yyyy-MM-dd');
    this.dailyPlanSummaryService.updateTaskSummary(newTaskSummary)
      .subscribe({
        next: (taskSummary) => {
          this.snackBarService.open('Task Summary Successfully updated', '' , {duration: 300});
          this.taskScheduleRepositoryService.updateTaskSummaryIdOfTaskSchedule(taskSummary.taskScheduleId, this.ProfileId);
          },
        error: (apiError: ApiError) => {this.messageBoxService.info('Task summary not updated',
          apiError.title, apiError.detail); }
      });

  }
}
