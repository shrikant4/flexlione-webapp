import {TaskModel} from '../../../shared/task-form/task.model';
import {TaskScheduleModel} from '../../schedule/task-schedule.model';


export class TaskSummaryModel {

  taskSummaryId = '';
  taskScheduleId = '';
  taskId = '';
  description ? = '';
  expectedOutput ? = '';
  expectedHour ? = 0;
  actualOutput?: string = null;
  actualHour ? = 0;
  date ? = '';
  task: TaskModel;
  stamp: string;
  action: string;
  taskSchedule: TaskScheduleModel;
  systemHours = 0;

}


