import {Component, OnInit, Input, ViewChild, OnDestroy} from '@angular/core';
import {DailyPlanSummaryService} from '../../Services/daily-plan-summary.service';
import {MessageBoxService} from '../../settings/message-box.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {DatePipe} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {TaskScheduleModel} from '../schedule/task-schedule.model';
import {MatAccordion} from '@angular/material/expansion';
import {TaskSummaryModel} from './task-summary/task-summary.model';
import {TaskScheduleManagementService} from '../../Services/task-schedule-management.service';
import {takeWhile} from 'rxjs/operators';
import {TaskScheduleRepositoryService} from '../../Services/task-schedule-repository.service';
import {ApiError} from '../../settings/api-error.model';





/** @title Form field appearance variants */
@Component({
  selector: 'app-profile-daily-plan-summary',
  templateUrl: './daily-plan-summary.component.html',
  styleUrls: ['../profile.component.css']
})
export class DailyPlanSummaryComponent implements  OnInit , OnDestroy {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  TaskScheduleList: TaskScheduleModel[] = [];
  DailyScheduleList: TaskScheduleModel[] = [];
  UpdatedTaskSummary: TaskSummaryModel = new TaskSummaryModel();
  TaskSummaryForm:  TaskSummaryModel[] ;
  DateSelected: Date = new Date();
  Today: Date = new Date();
  isAlive = true;
  public dateCheck = true;
  @Input()
  set profile(profileId: string) {
    this.ProfileId = profileId;
    console.log(this.ProfileId);
  }
  ProfileId: string;
  constructor(
    private searchManagementService: DailyPlanSummaryService,
    private messageBoxService: MessageBoxService,
    private store: Store<AppState>,
    private datePipe: DatePipe,
    private snackBarService: MatSnackBar,
    private route: ActivatedRoute,
    private  dailyPlanSummaryService: DailyPlanSummaryService,
    private taskScheduleManagementService: TaskScheduleManagementService,
    private router: Router,
    private taskScheduleRepositoryService: TaskScheduleRepositoryService
  ) {
  }
  changeEvents(calendarSelectedDate: Date): void {
   const scheduleListForSelectedDate = this.TaskScheduleList.filter((value) => {
      return (value.owner === this.ProfileId && new Date(value.date).toDateString() === new Date(calendarSelectedDate).toDateString());
    });
   // sorted by time of day
    this.DailyScheduleList = scheduleListForSelectedDate.sort((a, b) => a.startHour - b.startHour);
    if (new Date(new Date(calendarSelectedDate).toDateString()) >= new Date(this.Today.toDateString())) {
  this.dateCheck = false;
} else {
  this.dateCheck = true;
}
  }
  ngOnInit() {
    // Subscribing to observable  and taking action  when ever there is taskSchedule added
    this.taskScheduleRepositoryService.taskSchedulesInStore$.pipe(takeWhile(value => this.isAlive)).subscribe((taskScheduleList: TaskScheduleModel[]) => {
      this.TaskScheduleList = taskScheduleList;
      this.changeEvents(this.DateSelected);
    });
    // Once subscribed forcefully triggering an event that would pass a new reference in above observable, hence populate local TaskScheduleList
    this.taskScheduleRepositoryService.readTaskScheduleInStore(this.ProfileId);
    // Selected date from calendar view will be received as query param
    this.route.queryParams.subscribe({
      next: (param) => {
        if (param.calendarSelectedDate !== undefined) {
          this.DateSelected = param.calendarSelectedDate;
          this.changeEvents(param.calendarSelectedDate);
          this.router.navigate([], {
            queryParams: {
              'calendarSelectedDate': null
            },
            queryParamsHandling: 'merge'
          });
        }
      },
      error: (apiError: ApiError) => {this.messageBoxService.info('Error in daily plan summary',
        apiError.title, apiError.detail); }
    });
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  getTaskSummary(taskSchedule: TaskScheduleModel) {
    let newTaskSummaryModel: TaskSummaryModel = new TaskSummaryModel();
    // Assigned new model for config function in form to work
    this.dailyPlanSummaryService.getTaskSummaryById(taskSchedule.taskSummaryId)
      .subscribe({
        next: (taskSummaryModel) => {
          console.log(taskSummaryModel);
          if (taskSummaryModel === null) {
            newTaskSummaryModel.taskId = taskSchedule.taskId;
            newTaskSummaryModel.description = taskSchedule.description;
            newTaskSummaryModel.taskScheduleId = taskSchedule.taskScheduleId;
          } else {
            newTaskSummaryModel = taskSummaryModel; }
          newTaskSummaryModel.expectedHour = (taskSchedule.stopHour - taskSchedule.startHour)
                                            + (taskSchedule.stopMinute - taskSchedule.startMinute) / 60;
          newTaskSummaryModel.date = taskSchedule.date;
          this.UpdatedTaskSummary = newTaskSummaryModel;
          },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Error in getting task summary .', apiError.title, apiError.detail);
        }
      });
  }

  onDeleteTaskSchedule(taskScheduleId: string) {

    // First delete from DB. When successful, remove from store
   this.taskScheduleRepositoryService.removeTaskSchedule(taskScheduleId, this.ProfileId);
  }



}
