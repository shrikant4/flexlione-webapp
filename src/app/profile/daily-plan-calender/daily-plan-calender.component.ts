import { Component, Input, OnDestroy, OnInit} from '@angular/core';
import {CalendarEvent, CalendarView} from 'angular-calendar';
import {ProfileComponent} from '../profile.component';
import {TaskScheduleModel} from '../schedule/task-schedule.model';
import {Router} from '@angular/router';
import {takeWhile} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {TaskScheduleRepositoryService} from '../../Services/task-schedule-repository.service';

@Component({
  selector: 'app-profile-daily-plan-calender',
  templateUrl: 'daily-plan-calender.component.html'
})

export class DailyPlanCalenderComponent implements  OnInit, OnDestroy {
  @Input()
  set profile(profileId: string) {

  if (this.ProfileId !== undefined) { this.taskScheduleRepositoryService.fetchTaskSchedulesForMonthAndProfile(profileId, new Date().getMonth() + 1, new Date().getFullYear()); }
   this.ProfileId = profileId; }

  ProfileId: string;
  TaskScheduleList: TaskScheduleModel[] = [];
  today = new Date();
  viewDate: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 13, 0);
  view: CalendarView = CalendarView.Week;
  CalendarView = CalendarView;
  events: CalendarEvent[] = [];
  isAlive  = true;
  // Observable for Selected Day to Check Task Summary
  public selectedDayForTaskSummary = new Subject<Date>();
 //  Observer for Selected Day to Check Task Summary
  constructor(
    private profileComponent: ProfileComponent,
    private router: Router,
    private store: Store<AppState>,
     private taskScheduleRepositoryService: TaskScheduleRepositoryService
  ) {}


  ngOnInit() {
    // Subscribing to observable  and taking action  when ever there is taskSchedule added
    this.taskScheduleRepositoryService.taskSchedulesInStore$.pipe(takeWhile(value => this.isAlive)).subscribe((taskScheduleList: TaskScheduleModel[]) => {
      this.TaskScheduleList = taskScheduleList;
      this.changeEvents();
     } );
    // Update Schedules for current month, plus buffer - Previous and Next
    // Why Buffer? In order to avoid edge cases in week view at end of the month
    // Once above schedules are populated, we pass a new ref in above observables, and hence populate local task schedule list
    this.taskScheduleRepositoryService.fetchTaskSchedulesForMonthAndProfile(this.ProfileId, new Date().getMonth() + 1, new Date().getFullYear()).then(r => {
      // do Nothing
      });
  }
  ngOnDestroy() {
    // Stop subscribing to above observable when component is destroyed. Avoid data leakage.
    this.isAlive = false;

  }
  navigate(viewDate: Date) {
    console.log(viewDate);
    this.taskScheduleRepositoryService.fetchTaskSchedulesForMonthAndProfile(this.ProfileId, viewDate.getMonth() + 1, viewDate.getFullYear()).then(r => {
      // do Nothing
    });
  }
  setView(view: CalendarView) {
    this.view = view;
    console.log('month: ' +  this.viewDate.getMonth());
  }

  dayClickedInMonthView({ date }: { date: Date; events: CalendarEvent[] }): void {
    console.log(date);

    this.selectedDayForTaskSummary.next(date);
   this.router.navigateByUrl(this.router.url + '?calendarSelectedDate=' + date);
  }

  dayClickedInWeekView(evn): void {
    console.log(evn.day.date);

    this.selectedDayForTaskSummary.next(evn.day.date);
    this.router.navigateByUrl(this.router.url + '?calendarSelectedDate=' + evn.day.date);

  }
  changeEvents(): void {
  this.events = [];
  for (let j = 0; j < this.TaskScheduleList.length; j++) {
  this.events.push(this.getEventForScheduleTask(this.TaskScheduleList[j]));
  }
  }

  getEventForScheduleTask(taskSchedule: TaskScheduleModel): CalendarEvent {
    const date: Date = new Date(taskSchedule.date);
    const event: CalendarEvent = {
      start:  new Date(date.getFullYear(), date.getMonth(), date.getDate(),
        taskSchedule.startHour, taskSchedule.startMinute),
      title: taskSchedule.description,
      end:  new Date(date.getFullYear(), date.getMonth(), date.getDate(),
        taskSchedule.stopHour, taskSchedule.stopMinute),
    };
    return event;
  }
}
