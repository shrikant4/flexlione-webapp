import {Component, Input, Output, ViewChild, EventEmitter, OnInit} from '@angular/core';
import {UserService} from '../../core';
import {MatAccordion} from '@angular/material/expansion';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AddOrEditScheduleDialogComponent} from '../schedule/add-or-edit-schedule-dialog.component';

import { TaskScheduleModel} from '../schedule/task-schedule.model';
import {ActivatedRoute, Router} from '@angular/router';
import {SprintModel} from '../../sprint/sprint.model';
import {AddOrEditSprintDialogComponent} from './add-or-edit-sprint-dialog.component';
import {SprintManagementService} from '../../Services/sprint-management.service';
import {TaskModel} from '../../shared/task-form/task.model';
import {TaskManagementService} from '../../Services/task-management-service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProfileModel} from '../profile-models/profile.model';
import {ApiError} from '../../settings/api-error.model';
import {MessageBoxService} from '../../settings/message-box.service';
import {PageEvent} from '@angular/material/paginator';
import {ProfileManagementService} from '../../Services/profile-management.service';



@Component({
  selector: 'app-sprint-planner',
  templateUrl: './sprint-planner.component.html'
})
export class SprintPlannerComponent implements OnInit {
  public profileId: string;
  get profile(): string {
    return this.profileId;
  }
  @Input()
  set profile(value) {
    this.profileId = value;
  }
 @Input() sprintList: SprintModel[];
 @Output() newItemEvent  = new EventEmitter<string>();
  @ViewChild(MatAccordion) accordion: MatAccordion;
  public currentUser: ProfileModel;
  public selectedSprint: SprintModel = new SprintModel();
  sprintData: SprintModel [] = [];
  public sortedSprintData: SprintModel[];
  length = 100;
  pageSize = 5;
  pageIndex = 1;
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private dialog: MatDialog,
    private sprintManagementService: SprintManagementService,
    private router: Router,
    private taskManagementService: TaskManagementService,
    private snackBarService: MatSnackBar,
    private messageBoxService: MessageBoxService,
  ) {  }

  ngOnInit() {
    // Load the current user's data
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    }

  onUpdateOrScheduleNewTask(task: TaskModel): void {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      task : task
    };

    this.dialog.open(AddOrEditScheduleDialogComponent, dialogConfig)
      .afterClosed().subscribe({
      next: (taskSchedule: TaskScheduleModel) => {},
      error: (apiError: ApiError) => {
        this.messageBoxService.info('Error: Error in scheduling Task.', apiError.title, apiError.detail);
      }
    });
  }
  onRowClick(sprint: SprintModel) {
    this.selectedSprint = sprint;
    this.GetTaskList(sprint.sprintId);
  }
  GetTaskList(sprintId: string) {
    this.sprintManagementService.getSprintById(sprintId, 'plannedTask')
      .subscribe({
        next: (sprint) => {
          this.selectedSprint.plannedTasks = sprint.plannedTasks;
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error.', apiError.title, apiError.detail);
        }
      });
  }
  onAddOrEditSprint( isEdit: boolean, sprint?: SprintModel) {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    const newSprint: SprintModel = new SprintModel();
    newSprint.owner = this.profileId;
    dialogConfig.data = {

      isEdit: isEdit,
      sprint: sprint === undefined ? newSprint : sprint
    };
    this.dialog.open(AddOrEditSprintDialogComponent, dialogConfig).afterClosed().subscribe(
      {
        next: (newSprintModel: SprintModel) => {
          console.log('Added Sprint: ' + newSprintModel);
          this.newItemEvent.emit(newSprintModel.owner);
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Error in updating sprint.', apiError.title, apiError.detail);
        }
      }
    );

  }

  onShowSchedulesForTask(taskId: string): void {
    this.router.navigateByUrl(this.router.url + '?taskId=' + taskId);
    // Remove query params
  }

  onRemoveTaskFromSprint(task: TaskModel): void {
    this.taskManagementService.removeTaskFromSprint(task.taskId, this.onSuccess);
  }

  onSuccess = (task: TaskModel)  => {
    this.snackBarService.open('Task Successfully removed from sprint-planner', '', {duration: 300});
  }
}
