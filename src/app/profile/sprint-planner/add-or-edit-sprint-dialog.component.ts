import {Component, Inject, OnInit} from '@angular/core';
import {  MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { MessageBoxService } from '../../settings/message-box.service';
import {DatePipe} from '@angular/common';
import {ApiError} from '../../settings/api-error.model';
import {SprintManagementService} from '../../Services/sprint-management.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {ProfileStoreModel} from '../../shared/store/interfaces/profile-store.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProfileManagementService} from '../../Services/profile-management.service';
import {UserService} from '../../core';
import {ProfileModel} from '../profile-models/profile.model';
import {SprintModel} from '../../sprint/sprint.model';


@Component({
  selector: 'app-add-or-edit-sprint-planner-dialog',
  templateUrl: './add-or-edit-sprint-dialog.component.html',
  styleUrls: ['../profile.component.css']
})
export class AddOrEditSprintDialogComponent implements OnInit {
  UserList: ProfileStoreModel[] = [];
  private messageBoxService: MessageBoxService;
  public isEdit: boolean ;
  public taskId: string;
  public Owner: string;
  public scorePolicy: string[] = ['IncrementalScoreAllocationPolicy', 'BinaryScoreAllocationPolicy'];
  currentUser: ProfileModel;
  public loggedInUserId: string;
  public profileId = '';
  public clicked =  false;
  options: string[] = [];
  sprint: SprintModel[];
  newSprint: FormGroup;
  sprintData: SprintModel [] = [];
  public sortedSprintData: SprintModel[];
  length = 100;
  pageSize = 5;
  pageIndex = 1;

   constructor(private datePipe: DatePipe,
               public dialogRef: MatDialogRef<AddOrEditSprintDialogComponent>,
              messageBoxService: MessageBoxService,
               @Inject(MAT_DIALOG_DATA) data,
              private  store: Store<AppState>,
               private sprintManagementService: SprintManagementService,
              private  snackBarService: MatSnackBar,
               private profileManagementService: ProfileManagementService,
               private formBuilder: FormBuilder,
               private userService: UserService, ) {
    this.messageBoxService = messageBoxService;
    // Need to toggle Add/Update Button on Form
    this.isEdit = data.isEdit;
    this.Owner = data.sprint.owner;

     this.userService.currentUser.subscribe(
       (userData) => {
         this.currentUser = userData;
         this.loggedInUserId = this.currentUser.profileId;
       });
     if (this.isEdit) {
       this.newSprint = this.formBuilder.group(data.sprint);
     //  this.newSprint.addControl('scorePolicy', new FormControl());
       this.newSprint.patchValue({
         unplannedTasks: [],
       });

     } else {
     this.newSprint = this.formBuilder.group( new SprintModel()
     );
       this.newSprint.patchValue({
         owner: this.loggedInUserId,
         sprintId: 'Server Generated',
         fromDate: new Date(),
       });
     }
     this.newSprint.controls['sprintId'].disable();
     this.newSprint.controls['owner'].disable();
  }

  async ngOnInit() {
    this.dialogRef.updateSize('50%', '70%');
    const profiles = await this.profileManagementService.getAllProfiles().toPromise();
    this.UserList.length = 0;
    for (let i = 0; i < profiles.length; i++) {
      this.UserList.push(profiles[i]);
    }
  }
    onNoClick(): void {
    this.dialogRef.close();
  }

  onAddOrUpdate() {
     const newSprint = this.newSprint.getRawValue();
    newSprint.fromDate =  this.datePipe.transform(this.newSprint.getRawValue().fromDate, 'yyyy-MM-dd');
    newSprint.toDate =  this.datePipe.transform(this.newSprint.getRawValue().toDate, 'yyyy-MM-dd');
    this.sprintManagementService.AddOrUpdateSprint(newSprint)
      .subscribe({
        next: (sprint) => {
          this.clicked = true;
          this.snackBarService.open('Sprint successfully created', '', {duration: 300});
          this.dialogRef.close(sprint);
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Sprint not created .', apiError.title, apiError.detail);
        }
      });
  }

}

