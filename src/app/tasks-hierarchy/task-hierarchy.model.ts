export class TaskHierarchyModel {
  taskId: string;
  description: string;
  totalHoursSpent: number;
  totalEstimatedHours: string;
  childrenTaskIdList: string[];
  childrenTaskHierarchy?: TaskHierarchyModel[];
}

