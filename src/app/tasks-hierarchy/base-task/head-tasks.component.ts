import {Component, OnInit} from '@angular/core';
import { MatDialog} from '@angular/material/dialog';
import { MessageBoxService } from '../../settings/message-box.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {TaskManagementService} from '../../Services/task-management-service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';


@Component({
  selector: 'app-head-task',
  templateUrl: './head-tasks.component.html',
  styleUrls: ['../../app.component.css']
})
export class HeadTasksComponent implements OnInit {
  levelReceived: string;
  taskIdSelected: string;
  constructor(activatedRoute: ActivatedRoute, dialog: MatDialog, messageBoxService: MessageBoxService, snackBarService: MatSnackBar,
              taskManagementService: TaskManagementService, router: Router, private store: Store<AppState>) {
        this.router = router;
    activatedRoute.queryParams.subscribe({
      next: (params: Params) => {
        this.showInTaskTree('0', params.L0, params.L1, params.L2);
      }
    });
  }
    public selectedTaskId: string;
    private router: Router;

  sendL0HId: string;
  sendL1HId: string;
  sendL2HId: string;
  sendL3HId: string;

  sendL0TaskId: string;
  sendL1TaskId: string;
  sendL2TaskId: string;
  sendL3TaskId: string;

  levelAndIdReceivedFromChild(commonTaskEvent) {
    this.taskIdSelected = commonTaskEvent.id;
    this.levelReceived = commonTaskEvent.level;
    this.sendLevelsUpdate();
  }

  toggle() {
    this.router.navigateByUrl('/template');
  }

  sendLevelsUpdate() {
    switch (this.levelReceived) {
      case '0': {
        this.sendL1TaskId = this.taskIdSelected;
        this.sendL2TaskId = '';
        this.sendL3TaskId = '';
        break;
      }
      case '1': {
        this.sendL2TaskId = this.taskIdSelected;
          this.sendL3TaskId = '';
          break;
      }
      case '2': {
        this.sendL3TaskId = this.taskIdSelected;
        break;
      }
      case '3': {
        this.sendL1TaskId = this.sendL3TaskId;
        this.sendL2TaskId = this.taskIdSelected;
        this.sendL3TaskId = '';
        this.sendL1HId = this.taskIdSelected;
          break;
      }
    }
    //  Switch case statement is used when we have to choose between different blocks of code
    //  based on an expression. It replaces the need of multiple if...elseif conditions
  }
  showInTaskTree( l0: string, l1: string, l2: string, l3: string) {
    this.sendL0TaskId = l0;
    this.sendL1TaskId = l1;
    this.sendL2TaskId = l2;
    this.sendL3TaskId = l3;

    this.sendL2HId = l3;

  }

  ngOnInit(): void {
  }
}
