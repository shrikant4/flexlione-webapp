import {  NgModule } from '@angular/core';
import {HeadTasksComponent} from './base-task/head-tasks.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TaskHierarchyRoutingModule} from './task-hierarchy-routing.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatOptionModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {TaskHierarchyResolverService} from './task-hierarchy-resolver.service';
import { ChildTaskListComponent } from './child-task-list/child-task-list-component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatRadioModule} from '@angular/material/radio';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatPaginatorModule} from '@angular/material/paginator';




@NgModule({
    imports: [
        SharedModule,
        TaskHierarchyRoutingModule,
        MatMenuModule,
        MatIconModule,
        MatListModule,
        CommonModule,
        MatIconModule,
        MatMenuModule,
        MatListModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatSlideToggleModule,
        MatPaginatorModule
    ],
  declarations: [
    HeadTasksComponent,
    ChildTaskListComponent
  ],
  providers: [
    MatSnackBar,
    TaskHierarchyResolverService
  ]
})
export class TaskHierarchyModule {}
