import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HeadTasksComponent} from './base-task/head-tasks.component';
import {TaskHierarchyResolverService} from './task-hierarchy-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: HeadTasksComponent,
    resolve: {
      article: TaskHierarchyResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskHierarchyRoutingModule {}
