import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MessageBoxService} from '../../settings/message-box.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {TaskManagementService} from '../../Services/task-management-service';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {TaskModel} from '../../shared/task-form/task.model';
import {ApiError} from '../../settings/api-error.model';
import * as TaskActions from '../../shared/store/create-task.action';
import {CreateTaskStoreModel} from '../../shared/store/interfaces/create-task-store.model';



@Component({
  selector: 'app-common-task-hierarchy',
  templateUrl: './child-task-list-component.html',
  styleUrls: ['../../app.component.css']
})
export class ChildTaskListComponent implements OnInit {
  public receivedTaskId: string;
  public highlightId: string;
  @Input() setLevel;
  @Output() sendLevelAndId = new EventEmitter();

  get displayTaskId(): string {
    return this.receivedTaskId;
  }

  @Input()
  set displayTaskId(value) {
    this.receivedTaskId = value;
    if (this.receivedTaskId !== undefined && this.receivedTaskId !== '') {
      console.log(this.receivedTaskId);
            this.loadTaskList();
    }
  }
  get highlightTaskId(): string {
    return this.highlightId;
  }
  @Input()
  set highlightTaskId(value) {
    this.highlightId = value;
  }
  data: Array<any>;
  page: Number = 1;
  public selectedTaskId: string;
  private dialog: MatDialog;
  private messageBoxService: MessageBoxService;
  private snackBarService: MatSnackBar;
  private taskManagementService: TaskManagementService;
  public levelTask: TaskModel;
  currentDate = new Date().toISOString();
  public level: string;
  public parameter: Params;
public shortTaskList: { 'taskId': string, 'description': string, 'status': string, 'deadline': string }[];
  constructor(dialog: MatDialog,
              messageBoxService: MessageBoxService,
              snackBarService: MatSnackBar,
              activatedRoute: ActivatedRoute,
              taskManagementService: TaskManagementService,
              private router: Router,
              private store: Store<AppState>) {
    this.dialog = dialog;
    this.messageBoxService = messageBoxService;
    this.snackBarService = snackBarService;
    this.taskManagementService = taskManagementService;
    this.data = new Array<any>();
      }

  ngOnInit() {
    // each instance of this component will set its level by input value
    this.level = this.setLevel;
  }

  onRowClick(taskId: string): void {
    console.log(taskId);
    this.sendLevelAndId.emit({id: taskId, level: this.level });
    this.selectedTaskId = taskId;

  }

  loadTaskList() {
    this.taskManagementService.getTaskIdList(this.receivedTaskId, this.callback);

  }


  callback = (list: { 'taskId': string, 'description': string, 'status': string, 'deadline': any }[]) => {
   this.shortTaskList =  list;
console.log(list);
  }

  onAddNewTaskButtonClick(): void {
    this.store.dispatch(new TaskActions.RemoveCreateTask());
    const task: CreateTaskStoreModel = {parentTaskId: this.receivedTaskId};
    this.store.dispatch(new TaskActions.AddCreateTask(task));
    this.router.navigateByUrl('/editor');
  }

  onVisitTask(): void {
    this.router.navigateByUrl('/article/' + this.selectedTaskId);
  }

  onRemoveTaskButtonClick(taskId: string): void {
    this.messageBoxService.confirmWarn(
      'Are you sure you want to remove the task ' + taskId + '?', 'Remove')
      .afterClosed().subscribe({

      next: (proceed: boolean) => {
        if (proceed) {
          this.taskManagementService.removeTask(taskId).subscribe({
            next: () => {
              this.snackBarService.open('Task Successfully Removed.', '', {duration: 3000});
              this.loadTaskList();
            },
            error: (apiError: ApiError) => this.messageBoxService.info('Error: Failed to remove Task',
              apiError.title, apiError.detail)
          });
        }
      }
    });
  }
}
