
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// export keyword is same as public keyword in C# and Java. If export keyword is used, the class
// can used in other files.

@Injectable()
export class ServerConfigService {
  public router: Router;
  public base_url ;
  constructor(router: Router) {
    this.router = router;

  }



  getBaseUrl(): string {

    const currentUrl = window.location.href;
    if (currentUrl.includes('localhost')) {


      // this.base_url = 'https://localhost:5001/api/v1';
      // this.base_url = 'https://ptl-api-dev.flexli.in/flexlione-webapi-beta-staging/api/v1';
      this.base_url = 'https://ptl-api-dev.flexli.in/flexlione-webapi-alpha/api/v1';
    } else if (currentUrl.includes('beta-staging')) {
      this.base_url = 'https://ptl-api-dev.flexli.in/flexlione-webapi-beta-staging/api/v1';
    } else if (currentUrl.includes('beta')) {
      this.base_url = 'https://ptl-api-dev.flexli.in/flexlione-webapi-beta/api/v1';
      } else if (currentUrl.includes('alpha')) {
     this.base_url = 'https://ptl-api-dev.flexli.in/flexlione-webapi-alpha/api/v1';
     }
    return this.base_url;

  }
}




