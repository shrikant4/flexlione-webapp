import { SprintComponent } from './sprint.component';
import {NgModule} from '@angular/core';
import {SprintTaskSummaryComponent} from './sprint-task-summary/sprint-task-summary.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatTableModule} from '@angular/material/table';
import {SprintRoutingModule} from './sprint-routing.module';
import {CommonModule} from '@angular/common';
import {ArticleModule} from '../article/article.module';
import {MatMenuModule} from '@angular/material/menu';
import {SharedModule} from '../shared';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatDialogModule} from '@angular/material/dialog';
import {DialogSprintComponent} from './dialog-sprint/dialog-sprint.component';
import {MatSortModule} from '@angular/material/sort';
import { SprintReportComponent } from './sprint-report/sprint-report.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { UnplannedTaskScoreComponent } from './unplanned-task-score/unplanned-task-score.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';





@NgModule({
  declarations:
    [
      SprintComponent,
      SprintTaskSummaryComponent,
      DialogSprintComponent,
      SprintReportComponent,
      UnplannedTaskScoreComponent
    ],

    imports:
        [
            CommonModule,
            SprintRoutingModule,
            FormsModule,
            MatExpansionModule,
            MatButtonModule,
            MatIconModule,
            MatFormFieldModule,
            MatInputModule,
            DragDropModule,
            MatTableModule,
            ArticleModule,
            MatMenuModule,
            SharedModule,
            MatSelectModule,
            MatDatepickerModule,
            MatDialogModule,
            MatSortModule,
            MatPaginatorModule,
            MatProgressSpinnerModule,

        ],

  exports: [
    SprintComponent
  ]
})
export class SprintModule {}



