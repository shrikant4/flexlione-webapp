import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core';
import {SprintComponent} from './sprint.component';
import {SprintReportComponent} from './sprint-report/sprint-report.component';
const routes: Routes = [
  {
    path: '',
    component: SprintComponent,

  },
  {
    path: 'report/:sprintId',
    component: SprintReportComponent,
    canActivate: [AuthGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SprintRoutingModule {}
