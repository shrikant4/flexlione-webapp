import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnplannedTaskScoreComponent } from './unplanned-task-score.component';

describe('UnplannedTaskScoreComponent', () => {
  let component: UnplannedTaskScoreComponent;
  let fixture: ComponentFixture<UnplannedTaskScoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnplannedTaskScoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnplannedTaskScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
