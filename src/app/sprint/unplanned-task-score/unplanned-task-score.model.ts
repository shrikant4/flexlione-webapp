export class UnplannedTaskScoreModel {
  sprintId: string = '';
  taskId: string = '';
  hours: string = '';
  profileId: string = '';
}
