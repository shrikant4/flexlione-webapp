import {ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TaskModel} from '../../shared/task-form/task.model';
import {SprintManagementService} from '../../Services/sprint-management.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../Services/user.service';
import {error} from 'protractor';
import {UnplannedTaskScoreModel} from './unplanned-task-score.model';
import {ApiService} from '../../core';
import {MessageBoxService} from '../../settings/message-box.service';
import {ApiError} from '../../settings/api-error.model';

@Component({
  selector: 'app-unplanned-task-score',
  templateUrl: './unplanned-task-score.component.html',
  styleUrls: ['./unplanned-task-score.component.css']
})
export class UnplannedTaskScoreComponent implements OnInit {
  loggedInId: string;
  updateScore: FormGroup;
  unplannedTask: TaskModel;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private sprintManagementService: SprintManagementService,
              private userService: UserService,
              public dialogRef: MatDialogRef<UnplannedTaskScoreComponent>,
              private formBuilder: FormBuilder,
              private  messageBoxService: MessageBoxService,
              private changeDetectorRef: ChangeDetectorRef ) {

  }


  ngOnInit(): void {
    this.updateScore = this.formBuilder.group(new UnplannedTaskScoreModel());
    this.unplannedTask = this.data.unplannedTask;
    this.userService.currentUser.subscribe(
      (userData) => {
            this.loggedInId = userData.profileId;
      });
    this.updateScore.patchValue({
      sprintId: this.data.sprintId,
      taskId: this.data.unplannedTask.taskId,
      profileId: this.loggedInId
    });
  }
  request() {
this.sprintManagementService.updateScore(this.updateScore.value, 'request')
  .subscribe({next: (result) => { this.dialogRef.close(result);
    } ,
    error: (apiError: ApiError ) => { this.messageBoxService.info('Error:', apiError.title, );
    this.dialogRef.close(); } } );
     }

  approve() {
    this.sprintManagementService.updateScore(this.updateScore.value, 'approve')
      .subscribe({next: (result) => { this.dialogRef.close(result);
          },
        error: (apiError: ApiError ) => {this.messageBoxService.info('Error:', apiError.title, );
        this.dialogRef.close();
         }
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
