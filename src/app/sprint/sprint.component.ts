import {Component, OnInit} from '@angular/core';
import {ProfileManagementService} from '../Services/profile-management.service';
import {ProfileStoreModel} from '../shared/store/interfaces/profile-store.model';
import {UserService} from '../core';






@Component({
  selector: 'app-sprint-planner',
  templateUrl: './sprint.component.html',
  styleUrls: ['./sprint.component.css']
})
export class SprintComponent implements OnInit {
  users: string[] = [];
  public Profiles: ProfileStoreModel[] = [];
  public profileId = '';
  profileName = '';
  options: string[] = [];
  loggedInUser: string;

  constructor(private profileManagementService: ProfileManagementService,
              private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.profileId = userData.profileId; // Update  profile id and name
        this.loggedInUser = userData.profileId;
      }
    );
    this.profileManagementService.getAllProfiles()
      .subscribe({
        next: (profiles) => {
          this.Profiles = profiles;
          this.profileName = this.GetProfileName(this.profileId);
          for (let i = 0; i < this.Profiles.length; i++) {
            this.options.push(this.Profiles[i].name);
            this.users.push(this.Profiles[i].name);
          }
        },
        error: () => {
        }
      });
    this.updateProfile(this.profileId);
  }

  public GetProfileId(profileName: string): string {
    const profile = this.Profiles.filter(function (value) {
      return (value.name === profileName);
    });
    return profile[0] === undefined ? profileName : profile[0].profileId;
  }

  public GetProfileName(profileId: string): string {
    const profile = this.Profiles.filter(function (value) {
      return (value.profileId === profileId);
    });
    return profile[0] === undefined ? profileId : profile[0].name;
  }

  public updateProfile(profileName: string) {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.profileId = userData.profileId; // Update  profile id and name
        this.loggedInUser = userData.profileId;
      });
    this.profileId = this.GetProfileId(profileName);
    this.profileName = this.GetProfileName(this.profileId);
  }
}
