
import {TaskModel} from '../shared/task-form/task.model';

export class SprintModel {
  sprintId = '';

  owner = '';

  fromDate = '';

  toDate = '';

  score: 0 ;

  description = '';

  plannedTasks: TaskModel[];

  unPlannedTasks: TaskModel[];

  status: string;

  approved: boolean;

  scorePolicy: string[] = [];

}
