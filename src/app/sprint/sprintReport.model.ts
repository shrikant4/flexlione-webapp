
export class SprintReportModel {

sprintReportLineItemId: string;

  sprintId: string;

  taskId: string;

  checkListItemId: string;

  description: string;

  resultType: string;

  result: any;

  userComment: string;

  managerComment: string = '';

  approved: string = '';

  status: string;

  worstCase: number;

  bestCase: number;

  score: number;

  isEdit: boolean;

  taskDescription: string;
  }
