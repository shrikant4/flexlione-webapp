import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ChecklistManagementService} from '../../Services/checklist-management.service';
import {TaskModel} from '../../shared/task-form/task.model';
import {SprintManagementService} from '../../Services/sprint-management.service';
import {SprintReportModel} from '../sprintReport.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MessageBoxService} from '../../settings/message-box.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UserService} from '../../core';
import {ApiError} from '../../settings/api-error.model';
import {PageEvent} from '@angular/material/paginator';




@Component({
  selector: 'app-sprint-report',
  templateUrl: './sprint-report.component.html',
  styleUrls: ['./sprint-report.component.css']
})
export class SprintReportComponent implements OnInit {
  @Input() loggedInId;
  public profileId = '';
  loggedInUserID: string;
  public sprintId: string;
  task: TaskModel;
  public lineItems: SprintReportModel[] = [];
  public managerComment = '';
  public ApprovedValues: string[] = ['NoAction', 'True', 'False'];
  editForm: FormGroup;
  isEdit = true;

// MatPaginator Inputs
  length = 100;
  pageSize = 5;
  pageIndex = 1;
  // MatPaginator Output
  $event: PageEvent;

  constructor(private routeParams: ActivatedRoute,
              private route: ActivatedRoute,
              private checkListManagementService: ChecklistManagementService,
              private sprintManagementService: SprintManagementService,
              private messageBoxService: MessageBoxService,
              private snackBarService: MatSnackBar,
              private userService: UserService,
              private formBuilder: FormBuilder,
  ) {

    this.editForm = this.formBuilder.group(new SprintReportModel());
  }


loadSprintReportLineItems() {
  this.routeParams.params.subscribe(params => {
    this.sprintId = (params['sprintId']);
    console.log(this.sprintId);
    this.sprintManagementService.getSprintReportById(this.sprintId, this.pageIndex, this.pageSize )
      .subscribe({
        next: (sprintReportLineItems) => {
          this.lineItems = sprintReportLineItems;
          this.lineItems.forEach(element => element['isEdit'] = false);
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Could not load sprint report line-items.',
            apiError.title, apiError.detail);
        }
      });
    console.log(this.lineItems);
  });
}
  ngOnInit(): void {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.profileId =  userData.profileId; // Update  profile id and name
        this.loggedInUserID = userData.profileId;
      }
    );

    this.loadSprintReportLineItems();
  }

  onSave(sprintReport) {
    sprintReport.isEdit = false;

    sprintReport.managerComment = this.editForm.getRawValue().managerComment;
    sprintReport.approved = this.editForm.getRawValue().approved;

    this.sprintManagementService.reviewCheckList(sprintReport, this.loggedInUserID)

      .subscribe({

        next: (editConfirmReport) => {
          this.snackBarService.open('Response Saved', '', {duration: 3000});
          this.loadSprintReportLineItems();
          console.log('Response saved');
          console.log(editConfirmReport);
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Could not save.', apiError.title, apiError.detail);
        }
      });
    this.editForm.reset();

  }

  onEdit(checkListItem) {
    const setValue = { approved : checkListItem.approved,
      managerComment: checkListItem.managerComment
    };
    this.editForm.patchValue(setValue);
    checkListItem.isEdit = true;

  }

  updateList(pageEvent: PageEvent) {
    this.pageIndex = pageEvent.pageIndex + 1 ;
    this.pageSize = pageEvent.pageSize;
    this.loadSprintReportLineItems();
  }
}
