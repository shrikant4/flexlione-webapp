import {Component, Inject, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import {SprintManagementService} from '../../Services/sprint-management.service';
import {SprintModel} from '../sprint.model';
import {TaskSummaryModel} from '../../profile/daily-plan-summary/task-summary/task-summary.model';
import {DailyPlanSummaryService} from '../../Services/daily-plan-summary.service';
import {ProfileManagementService} from '../../Services/profile-management.service';
import {TaskHierarchyManagementService} from '../../Services/task-hierarchy-management.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {DialogSprintComponent} from '../dialog-sprint/dialog-sprint.component';
import {ApiError} from '../../settings/api-error.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MessageBoxService} from '../../settings/message-box.service';
import {PageEvent} from '@angular/material/paginator';
import {CheckListItem} from '../../shared/checklist/check-list-item.model';
import {ShowChecklistComponent} from '../../shared/checklist/show-checklist/show-checklist.component';
import {ChecklistManagementService} from '../../Services/checklist-management.service';
import {UnplannedTaskScoreComponent} from '../unplanned-task-score/unplanned-task-score.component';
import {TaskModel} from '../../shared/task-form/task.model';




@Component({
  selector: 'app-sprint-task-summary',
  templateUrl: './sprint-task-summary.component.html',
  styleUrls: ['./sprint-task-summary.component.css'],
})


export class SprintTaskSummaryComponent implements OnInit, OnChanges {

  @Input() loggedInId;
  @Input() pId;
   length = 100;
   pageSize = 2;
   pageIndex = 1;
disableButtons = true;
  pageSizeOptions: number[] = [5, 10, 25, 100];
    // MatPaginator Output
  $event: PageEvent;
  panelOpenState = false;
  sprintData: SprintModel [] = [];
  summaryList: TaskSummaryModel [];
  profile: string;
  sprintStart: string;
  sprintClose: string;
    public detailedPath: string;
  public sortedSprintData: SprintModel[];
  constructor(private sprintManagementService: SprintManagementService,
              private dailyPlanSummaryService: DailyPlanSummaryService,
              private profileManagementService: ProfileManagementService,
              private taskHierarchyManagementService: TaskHierarchyManagementService,
              private checklistManagementService: ChecklistManagementService,
              public dialog: MatDialog,
              public dialogRef: MatDialogRef<ShowChecklistComponent>,
              private snackBarService: MatSnackBar,
              private  messageBoxService: MessageBoxService,
              @Inject(MAT_DIALOG_DATA) public data: any,

  ) {

  }

  ngOnInit(): void {

    this.detailedPath = window.location.href + '/report/';

    // this.profileManagementService.getAllProfiles()
    //
    //   .subscribe({
    //     next: (profiles) => {
    //       this.Profiles = profiles;
    //       this.profileName = this.GetProfileName(this.profileId);
    //       for (let i = 0; i < this.Profiles.length; i++) {
    //         this.options.push(this.Profiles[i].name);
    //       }
    //     },
    //     error: (apiError: ApiError) => {
    //       this.messageBoxService.info('Error!', apiError.title, apiError.detail);
    //     }
    //   });
    //

  }
  public getTaskSummaryForTask(taskId: string, fromDate: string, toDate: string) {this.dailyPlanSummaryService.getTaskSummaryByTaskId(taskId, 'allChildren', this.onSuccess);
  this.sprintStart = fromDate;
  this.sprintClose = toDate;
  }

  onSuccess = (taskSummaryList: TaskSummaryModel[]) => {
    this.summaryList = taskSummaryList.filter(x => new Date (x.date) >= new Date( this.sprintStart ) && new Date(x.date) <= new Date(this.sprintClose));

    console.log(this.summaryList);
    console.log(this.sprintStart);
      }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.sprintData, event.previousIndex, event.currentIndex);
  }


  ngOnChanges(changes: SimpleChanges): void {
      if (changes['pId'] !== undefined ) {
      this.profile = changes['pId'].currentValue;
      console.log(this.profile);
      this.sprintData.length = 0;
      this.loadSprintData();
    }
    console.log(this.sprintData);
  }
  onRequestSprintForApproval(sprint: SprintModel) {

    const dialogRef = this.dialog.open(DialogSprintComponent, {data: {message: 'to send a sprint-planner for approval?'}});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.sprintManagementService.RequestForApproval(sprint.sprintId, this.loggedInId)
          .subscribe({
            next: (sprintModel) => {
              this.snackBarService.open('request for ' + sprint.sprintId + ' has been sent by userid ' + this.pId, '' , {duration: 3000});
              console.log(sprintModel.sprintId);
              this.loadSprintData();
            },
            error: (apiError: ApiError) => {this.messageBoxService.info('Error: Could not sent request',
              apiError.title, apiError.detail); }
          });
      }
    });
  }

  onApproveSprint(sprint: SprintModel) {

    const dialogRef = this.dialog.open(DialogSprintComponent, {data: {message: 'to approve a sprint-planner?'}});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.sprintManagementService.ApproveSprint(sprint.sprintId, this.loggedInId)
          .subscribe({
            next: (sprintModel) => {
              this.snackBarService.open('Sprint approved', '' , {duration: 3000});
              console.log(sprintModel.sprintId);
              this.loadSprintData();
            },
            error: (apiError: ApiError) => {this.messageBoxService.info('Error: Failed to approve sprint',
              apiError.title, apiError.detail); }
          });
      }
    });
  }

  onRequestForClosure(sprint: SprintModel) {
    console.log('Request for closure sent' + this.loggedInId);

    const dialogRef = this.dialog.open(DialogSprintComponent, {data: {message: 'to send a request to close a sprint-planner?'}});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.sprintManagementService.RequestForClosure(sprint.sprintId, this.loggedInId)
          .subscribe({
            next: (sprintModel) => {
              this.snackBarService.open('Closure request has been sent', '' , {duration: 3000});
              console.log(sprintModel.sprintId);
              this.loadSprintData();
            },
            error: (apiError: ApiError) => {this.messageBoxService.info('Error: Could not sent closure request',
              apiError.title, apiError.detail); }
          });
      }
    });
  }

  onCloseSprint(sprint: SprintModel) {
    console.log('Sprint Closed' + this.loggedInId);

    const dialogRef = this.dialog.open(DialogSprintComponent, {data: {message: 'to close a sprint-planner?'}});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.sprintManagementService.CloseSprint(sprint.sprintId, this.loggedInId)
          .subscribe({
            next: (sprintModel) => {
              this.snackBarService.open('Sprint closed', '', {duration: 3000});
              console.log(sprintModel.sprintId);
              this.loadSprintData();
            },
            error: (apiError: ApiError) => {
              this.messageBoxService.info('Error: Failed to close sprint', apiError.title, apiError.detail);
            }
          });
      }
    });
  }

    onFinishReview(sprint: SprintModel) {
    console.log('Review completed');

    const dialogRef = this.dialog.open(DialogSprintComponent, {data: {message: 'to finish a review?'}});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.sprintManagementService.ReviewCompleted(sprint.sprintId, this.loggedInId)
          .subscribe({
            next: (sprintModel) => {
              this.snackBarService.open('Review completed', '', {duration: 3000});
              console.log(sprintModel.sprintId);
              this.loadSprintData();
            },
            error: (apiError: ApiError) => {
              this.messageBoxService.info('Error: Error in Finishing Review',
                apiError.title, apiError.detail);
            }
          });
      }

    });
  }
  loadSprintData() {
    if (this.profile !== undefined && this.profile !== null) {
      this.sprintManagementService.getSprintByProfileId(this.profile, ['plannedTask', 'unPlannedTask'], this.pageIndex, this.pageSize)
      .subscribe({next: (sprints) => {
        console.log(sprints);
          this.sprintData = sprints;
          this.sortedSprintData = this.sprintData.sort((a, b) => (new Date(b.fromDate).valueOf() - new Date(a.fromDate).valueOf()));
this.disableButtons  = false;
        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Error in loading sprint data.',
            apiError.title ,  apiError.detail);
        }
      });
  } }

  updateList(pageEvent: PageEvent) {
    this.disableButtons = true;
    this.sortedSprintData.length = 0;
    console.log(pageEvent);
    this.pageIndex = pageEvent.pageIndex + 1 ;
    this.pageSize = pageEvent.pageSize;
   this.loadSprintData();
  }
  onOpen(taskId: string) {
    console.log('button clicked');
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      typeId : taskId,
       isEdit: true,
       checkListType: 'Task'
    };
    this.dialog.open(ShowChecklistComponent, dialogConfig).afterClosed()
      .subscribe({
        next: (checkListItem: CheckListItem) => {
          if (checkListItem == null) { // Cancel button clicked
            return;
          }

        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error: Failed to update Template.',
            apiError.title, apiError.detail);
        }
      });
  }

  requestScore(sprintId: string, unplannedTask: TaskModel) {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      unplannedTask : unplannedTask,
      sprintId: sprintId,
          };
    this.dialog.open(UnplannedTaskScoreComponent, dialogConfig).afterClosed().subscribe(
      {next: (result) => {
          if (result != null) {
            this.snackBarService.open(' Hours Updated for task id' + result.taskId, '', {duration: 3000});
            this.sortedSprintData.length = 0;
            this.disableButtons = true;
            this.loadSprintData();
          }
        }
  });
}
}
