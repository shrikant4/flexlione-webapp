import { SearchTaskViewStoreModel } from './shared/store/interfaces/search-task-view-store.model';
import {CreateTaskStoreModel} from './shared/store/interfaces/create-task-store.model';
import {ProfileStoreModel} from './shared/store/interfaces/profile-store.model';

import {SearchQuery} from './shared/search-filter/search-filter.model';

import {TaskScheduleModel} from './profile/schedule/task-schedule.model';




export interface AppState {
  readonly searchTaskView: SearchTaskViewStoreModel[];
  readonly createTask: CreateTaskStoreModel[];
  readonly profile: ProfileStoreModel[];
  readonly taskSchedule: TaskScheduleModel[];
  readonly searchQuery: SearchQuery[];

}
