import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject} from 'rxjs';
import {ServerConfigService} from '../settings/server-config.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MessageBoxService} from '../settings/message-box.service';
import {AppState} from '../app.state';
import {Store} from '@ngrx/store';
import * as SearchQueryActions from '../shared/store/search-query.action';
import {SearchQuery} from '../shared/search-filter/search-filter.model';

// export keyword is same as public keyword in C# and Java. If export keyword is used, the class
// can used in other files.

@Injectable()
export class SearchQueryRepositoryService {

  // Special type of Observable that can emit an event. Observables that listens to below variable would have a new ref each time an event is emiited
  private searchQueryInStore = new Subject<SearchQuery[]>();
  // Observable string streams
  public searchQueryInStore$ = this.searchQueryInStore.asObservable();
  // Subscribing to current state of task schedules in store
  SearchQueryInStore: SearchQuery[] = [];


  constructor(private http: HttpClient,
              private serverConfigService: ServerConfigService,
              private snackBarService: MatSnackBar,
              messageBoxService: MessageBoxService,
              private store: Store<AppState>,
  ) {
    this.store.select('searchQuery').subscribe({next: (searchQuery) => {
        this.SearchQueryInStore = searchQuery;
      }});
  }

  // Forcefully triggering an event that would pass a new reference in local observable, hence populate local variable in the component that subscribes to it
  public readSearchQueryInStore() {
    this.emitSearchQueryInStore(this.SearchQueryInStore);
  }
  public  updateSearchQueryInStore(searchQuery: SearchQuery) {
    // Need to check availability against following months
    this.store.dispatch(new SearchQueryActions.AddSearchQuery(searchQuery));
    this.readSearchQueryInStore();
  }
  private emitSearchQueryInStore(revisedSearchQuery: SearchQuery[]) {
    this.searchQueryInStore.next(revisedSearchQuery);
  }



}
