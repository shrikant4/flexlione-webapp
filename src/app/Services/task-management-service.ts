import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskModel } from '../shared/task-form/task.model';
import {catchError, retry} from 'rxjs/operators';
import {ServerConfigService} from '../settings/server-config.service';
import {HandlerError} from '../settings/handle-error.service';
import {ApiError} from '../settings/api-error.model';
import {MessageBoxService} from '../settings/message-box.service';
import {AppState} from '../app.state';
import {Store} from '@ngrx/store';
import * as TaskScheduleActions from '../shared/store/task-schedule.action';
import {TaskScheduleModel} from '../profile/schedule/task-schedule.model';



// export keyword is same as public keyword in C# and Java. If export keyword is used, the class
// can used in other files.

@Injectable()
export class TaskManagementService {


  // trailing underscore is a naming convention for private variables of the class.
  private http_: HttpClient;

  private baseUrl: string ;
   constructor(http: HttpClient, serverConfigService: ServerConfigService, private messageBoxService: MessageBoxService, private store: Store<AppState> ) { // pass by reference
     this.http_ = http;
     this.baseUrl = serverConfigService.getBaseUrl();
   }
   dispatch(taskScheduleList: TaskScheduleModel[]) {
     this.store.dispatch(new TaskScheduleActions.AddAllTaskSchedule(taskScheduleList));
   }
   // Returns an observable for list of Line Items
  getTaskById(taskId: string, include: string): Observable<TaskModel> {

    const httpHeaders = {
      'Content-Type': 'application/json',
       'accept': 'application/json;v=1.0'
    };

    let queryStringParams;
    queryStringParams = {
        include: include,
        taskId: taskId
    };
    return this.http_.get<TaskModel>(this.baseUrl + '/Task/GetTaskById', { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  // Returns an observable for list of Line Items
  getTaskIdList(taskId: string, callback: (taskIdList: {'taskId': string, 'description': string}[]) => any): any {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    let queryStringParams;
    queryStringParams = {
      taskId: taskId
    };
    return this.http_.get<{'taskId': string, 'description': string}[]>(this.baseUrl + '/Task/GetTaskIdList', { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)

      ).subscribe({
        next : (taskIdList) => {
          callback(taskIdList);
        },
        error : (apiError: ApiError) => {this.messageBoxService.info(
          'Task Id List Not Received', apiError.title, apiError.detail);
        }
      });
  }
  removeTask(taskId: string): Observable<void> {

    const httpHeaders = {
      'Content-Type': 'application/json',
    };
    const queryStringParams = {
      taskId: taskId
    };
    return this.http_.post<void>(this.baseUrl + '/Task/RemoveTask', ' ' , { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  createOrUpdateTask(task: TaskModel, loggedInId: string): Observable<TaskModel> {

    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    const queryStringParams = {
      loggedInId: loggedInId
    };
    return this.http_.put<TaskModel>(this.baseUrl + '/Task/CreateOrUpdateTask', task, {params: queryStringParams, headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }
  removeTaskFromSprint(taskId: string, callback: (task: TaskModel) => any) {

    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    const queryStringParams = {
      taskId: taskId
    };

    return this.http_.put<TaskModel>(this.baseUrl + '/Task/RemoveTaskFromSprint', '', {params: queryStringParams, headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      ).subscribe({
        next : (task) => {
          callback(task);
        },
        error : (apiError: ApiError) => {
          this.messageBoxService.info('Error in removing Task', apiError.title, apiError.detail);
        }
      });
  }

  onLinkTaskToSprint(sprintId: string, taskId: string, callback: (task: TaskModel) => any) {
    this.linkTaskToSprint(sprintId, taskId)
      .subscribe({
        next: (task) => {
          callback(task);

        },
        error: (apiError: ApiError) => {
          this.messageBoxService.info('Error in linking task', apiError.title, apiError.detail);
        }
      });
  }

  linkTaskToSprint(sprintId: string, taskId: string): Observable<TaskModel> {

    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    const queryStringParams = {
      sprintId: sprintId,
      taskId: taskId
    };

    return this.http_.put<TaskModel>(this.baseUrl + '/Task/LinkTaskToSprint', '', {params: queryStringParams, headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  addLabelToTask(taskId: string, label: string): Observable<TaskModel> {

    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    const queryStringParams = {
      taskId: taskId,
      label: label
    };

    return this.http_.put<TaskModel>(this.baseUrl + '/Task/AddLabelToTask', '', {params: queryStringParams, headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }
}


