import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {catchError, retry} from 'rxjs/operators';
import {ServerConfigService} from '../settings/server-config.service';
import {HandlerError} from '../settings/handle-error.service';
import {ApiError} from '../settings/api-error.model';
import {MessageBoxService} from '../settings/message-box.service';
import {TaskHierarchyModel} from '../tasks-hierarchy/task-hierarchy.model';





// export keyword is same as public keyword in C# and Java. If export keyword is used, the class
// can used in other files.

@Injectable()
export class TaskHierarchyManagementService {


  // trailing underscore is a naming convention for private variables of the class.
  private http_: HttpClient;
  private baseUrl: string;
  public url = 'http://localhost:3000/posts';


  constructor(http: HttpClient, serverConfigService: ServerConfigService, private messageBoxService:
    MessageBoxService, ) { // pass by reference
    this.http_ = http;
    this.baseUrl = serverConfigService.getBaseUrl();

  }

  // Returns an observable for list of Line Items
  getTaskHierarchyByTaskId(taskId: string, include: string, callback: (taskHierarchy: TaskHierarchyModel) => any): any {

    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };

    let queryStringParams;
    queryStringParams = {
      include: include,
      taskId: taskId
    };
    this.http_.get<TaskHierarchyModel>(this.baseUrl + '/Task/GetTaskHierarchyByTaskId', {params: queryStringParams, headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      ).subscribe({
        next: (taskHierarchy) => {
          return callback(taskHierarchy);
        },
        error: (apiError: ApiError) => {
          // In front end let this error die silently
          // this.messageBoxService.info('Error in receiving Task Hierarchy',
          // apiError.title, apiError.detail);
        }
      });
  }
}
