import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {ServerConfigService} from '../settings/server-config.service';
import {HandlerError} from '../settings/handle-error.service';
import { SearchQuery} from '../shared/search-filter/search-filter.model';
import {SearchTaskViewStoreModel} from '../shared/store/interfaces/search-task-view-store.model';
import {DatePipe} from '@angular/common';
import {getStatusList} from '../shared/shared-lists/status-list';
import {TaskModel} from '../shared/task-form/task.model';
import {Template} from '../template-tasks/template.model';

// export keyword is same as public keyword in C# and Java. If export keyword is used, the class
// can used in other files.

@Injectable()
export class SearchManagementService {

  // trailing underscore is a naming convention for private variables of the class.
  private http_: HttpClient;
  private baseUrl: string ;
  private datepipe: DatePipe;

  constructor(http: HttpClient, serverConfigService: ServerConfigService, datepipe: DatePipe) { // pass by reference
    this.http_ = http;
    this.baseUrl = serverConfigService.getBaseUrl();
    this.datepipe = datepipe;
  }
  // Returns an observable for list of Task Detail Model Items.
  // Angular maps Task Detail model into search-filter Task Model
  getTaskSearchList(search: SearchQuery, pageIndex, pageSize): Observable<SearchTaskViewStoreModel[]> {
    const queryStringParams = {
      pageIndex: pageIndex,
      pageSize: pageSize
    };
    const httpHeaders = {
      'Content-Type': 'application/json',
       'accept': 'application/json'
    };
    return this.http_.put<SearchTaskViewStoreModel[]>(this.baseUrl + '/Search/SearchByQuery', search, {params: queryStringParams, headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }
  getSearchResultForTask(query: string, pageIndex, pageSize): Observable<SearchTaskViewStoreModel[]> {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json'
    };
   const queryStringParams = {
      searchQuery: query,
      pageIndex: pageIndex,
      pageSize: pageSize
    };
    return this.http_.post<SearchTaskViewStoreModel[]>(this.baseUrl + '/Search/GetSearchResultForTasks', '', {params: queryStringParams,  headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );

  }

  getSearchResultForTemplate (query: string, pageIndex, pageSize): Observable<Template[]> {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json'
    };
    const queryStringParams = {
      searchQuery: query,
      pageIndex: pageIndex,
      pageSize: pageSize
    };
    return this.http_.post<Template[]>(this.baseUrl + '/Search/GetSearchResultForTemplates', '', {params: queryStringParams,  headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );

  }

  getGlobalSearch(): SearchQuery {
   const query = new SearchQuery();
   query.deadline = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
   query.status = getStatusList().filter(function (status) {
     return status !== 'completed';
   });
    return query;
  }

  getPersonalSearch(profileId: string): SearchQuery {

    const query =   new SearchQuery();
    query.deadline = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
    query.status = getStatusList().filter(function (status) {
      return status !== 'completed';
    });
    query.assignedTo = [profileId];

    return query;
  }

  getSingleSearch(Description: string): SearchQuery {
  const query = new SearchQuery();
  query.description = Description ;
  return query;


  }

  addTag(newTag: string, tagType: string): Observable<any>  {
    const httpHeaders = {
      'Content-Type': 'application/json',
    };
    let queryStringParams;
    queryStringParams = {
      tag: newTag,
      tagType: tagType
    };
    return this.http_.put<any>(this.baseUrl + '/Tag/AddTag', newTag , {params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }
}
