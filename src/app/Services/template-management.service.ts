import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ServerConfigService} from '../settings/server-config.service';
import {Observable} from 'rxjs';
import {TaskModel} from '../shared/task-form/task.model';
import {catchError, retry} from 'rxjs/operators';
import {HandlerError} from '../settings/handle-error.service';
import {Template} from '../template-tasks/template.model';

@Injectable({
  providedIn: 'root'
})
export class TemplateManagementService {
  private http_: HttpClient;
  private baseUrl: string ;
  constructor(
    http: HttpClient,
    serverConfigService: ServerConfigService,
  ) {
    this.http_ = http;
    this.baseUrl = serverConfigService.getBaseUrl();
  }

  getTemplateById( templateId: string, include: string) {

    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };


    let queryStringParams;
    if (templateId == null) {
      queryStringParams = {
        include: include
      };
    } else {
      queryStringParams = {
        include: include,
        templateId: templateId
      };
    }

    return this.http_.get<Template>(this.baseUrl + '/Template/GetTemplateById', {params: queryStringParams,  headers: httpHeaders});

  }

  getSimilarTemplateList( templateId: string, pageIndex, pageSize) {

    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    let queryStringParams;
    queryStringParams = {
      templateId: templateId,
      pageIndex: pageIndex,
      pageSize: pageSize
    };
    return this.http_.get<Template[]>(this.baseUrl + '/Template/GetSimilarTemplateList', {params: queryStringParams,  headers: httpHeaders});
  }

  createOrUpdateTemplate(template: Template) {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    return this.http_.post<Template>(this.baseUrl + '/Template/CreateOrUpdateTemplate', template, { headers: httpHeaders} );

  }

  replaceChildTemplate(oldTemplateId: string, newTemplateId: string, parentTemplateId: string) {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    let queryStringParams;
    queryStringParams = {
      oldTemplateId: oldTemplateId,
      newTemplateId: newTemplateId,
      parentTemplateId: parentTemplateId,
    };
    return this.http_.post<Template>(this.baseUrl + '/Template/ReplaceChildTemplate', '', {params: queryStringParams, headers: httpHeaders });

  }


  addChildTemplate(childTemplateId: string, parentTemplateId: string): Observable<Template> {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    const queryStringParams = {
      childTemplateId: childTemplateId,
      parentTemplateId: parentTemplateId
    };
    return this.http_.post<Template>(this.baseUrl + '/Template/AddChildTemplate', '' , { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  removeChildTemplate(childTemplateId: string, parentTemplateId: string): Observable<Template> {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    const queryStringParams = {
      childTemplateId: childTemplateId,
      parentTemplateId: parentTemplateId
    };
    return this.http_.post<Template>(this.baseUrl + '/Template/RemoveChildTemplate', '' , { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  cloneTemplate(templateId: string) {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    const queryStringParams = {
      templatedId: templateId,
    };
    return this.http_.put<Template>(this.baseUrl + '/Template/CloneTemplate', '' , { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }
  generateTaskFromTemplate(template: Template): Observable<TaskModel> {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    return this.http_.post<TaskModel>(this.baseUrl + '/Template/GenerateTaskFromTemplate', template , { headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  getAllRolesForTemplateId(templateId: string, include: string = null ): Observable<string[]> {
    const httpHeaders = {
      'Content-Type': 'application/json',
      'accept': 'application/json;v=1.0'
    };
    const queryStringParams = {
      templateId: templateId,
      include: include

    };
    return this.http_.post<string[]>(this.baseUrl + '/Template/GetAllRolesForTemplateId', '' , { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }



}
