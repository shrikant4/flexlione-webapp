import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {catchError, retry, shareReplay} from 'rxjs/operators';
import {ServerConfigService} from '../settings/server-config.service';
import {HandlerError} from '../settings/handle-error.service';
import {ProfileModel} from '../profile/profile-models/profile.model';
import {ProfileStoreModel} from '../shared/store/interfaces/profile-store.model';
import {SearchTaskViewStoreModel} from '../shared/store/interfaces/search-task-view-store.model';


// export keyword is same as public keyword in C# and Java. If export keyword is used, the class
// can used in other files.

@Injectable()
export class ProfileManagementService {


  // trailing underscore is a naming convention for private variables of the class.
  private http_: HttpClient;
  private baseUrl: string ;
  private allUsers$: Observable<ProfileStoreModel[]>;




  constructor(http: HttpClient, serverConfigService: ServerConfigService) { // pass by reference
    this.http_ = http;
    this.baseUrl = serverConfigService.getBaseUrl();
  }
  // Returns an observable for list of Line Items
  getProfileById(profileId: string, include: string): Observable<ProfileModel> {

    const httpHeaders = {
      'Content-Type': 'application/json',
    };

    const queryStringParams
      = {
      include: include,
      profileId: profileId
    };


    return this.http_.get<ProfileModel>(this.baseUrl + '/Profile/GetProfileById', { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  getAllProfiles(): Observable<ProfileStoreModel[]> {
    if (!this.allUsers$) {
      const httpHeaders = {
        'Content-Type': 'application/json',
      };
// all user profile data will be stored once in allUser stream and send without calling to server.
      this.allUsers$ = this.http_.get<ProfileStoreModel[]>(this.baseUrl + '/Profile/GetAllProfiles', {headers: httpHeaders})
        .pipe(retry(1),
          catchError(HandlerError.handleError),
          shareReplay()
        );
    }
    return this.allUsers$;
  }

  addOrUpdateProfile(profileModel: ProfileModel): Observable<ProfileModel> {
    const httpHeaders = {
      'Content-Type': 'application/json',
    };

    return this.http_.post<ProfileModel>(this.baseUrl + '/Profile/AddOrUpdateProfile', profileModel, {headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );

  }

  filterbyTags(profileId: string, include: string[], pageIndex, pageSize): Observable<SearchTaskViewStoreModel[]> {

    const httpHeaders = {
      'Content-Type': 'application/json',
    };

    const queryStringParams
      = {
      include: include,
      profileId: profileId,
      pageIndex: pageIndex,
      pageSize: pageSize
    };


    return this.http_.get<SearchTaskViewStoreModel[]>(this.baseUrl + '/Search/SearchByProfileId', { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

}
