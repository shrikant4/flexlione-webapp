import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {ServerConfigService} from '../settings/server-config.service';
import {HandlerError} from '../settings/handle-error.service';
import {SprintModel} from '../sprint/sprint.model';
import {SprintReportModel} from '../sprint/sprint-report/sprint-report.model';
import {UnplannedTaskScoreModel} from '../sprint/unplanned-task-score/unplanned-task-score.model';


// export keyword is same as public keyword in C# and Java. If export keyword is used, the class
// can used in other files.

@Injectable()
export class SprintManagementService {


  // trailing underscore is a naming convention for private variables of the class.
  private http_: HttpClient;
  private baseUrl: string ;

  constructor(http: HttpClient, serverConfigService: ServerConfigService) { // pass by reference
    this.http_ = http;
    this.baseUrl = serverConfigService.getBaseUrl();
  }
  // Returns an observable for list of Line Items
  getSprintById(sprintId: string, include: string): Observable<SprintModel> {

    const httpHeaders = {
      'Content-Type': 'application/json'
    };

    const queryStringParams
      = {
      include: include,
      sprintId: sprintId,
          };


    return this.http_.get<SprintModel>(this.baseUrl + '/Sprint/GetSprintById', { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  AddOrUpdateSprint(sprintModel: SprintModel): Observable<SprintModel> {

    const httpHeaders = {
      'Content-Type': 'application/json'
    };

    return this.http_.post<SprintModel>(this.baseUrl + '/Sprint/AddOrUpdateSprint', sprintModel, {headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

    RequestForApproval(sprintId: string, userId: string): Observable<SprintModel> {

    const  httpHeaders = {
      'Content-Type': 'application/json'

    };
    const queryStringParams
        = {
      sprintId: sprintId,
      userId: userId
      };

    return this.http_.post<SprintModel>(this.baseUrl + '/Sprint/RequestForApproval', '', { params: queryStringParams, headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );

    }

    ApproveSprint (sprintId: string, approverId: string): Observable<SprintModel> {

      const  httpHeaders = {
        'Content-Type': 'application/json'

      };
      const queryStringParams
        = {
        sprintId: sprintId,
        approverId: approverId
      };

      return this.http_.post<SprintModel>(this.baseUrl + '/Sprint/ApproveSprint', '', { params: queryStringParams, headers: httpHeaders})
        .pipe(
          retry(1),
          catchError(HandlerError.handleError)
        );

    }

   RequestForClosure (sprintId: string, userId: string): Observable<SprintModel> {

     const  httpHeaders = {
       'Content-Type': 'application/json'

     };
     const queryStringParams
       = {
       sprintId: sprintId,
       userId: userId,
     };

     return this.http_.post<SprintModel>(this.baseUrl + '/Sprint/RequestForClosure', '', { params: queryStringParams, headers: httpHeaders})
       .pipe(
         retry(1),
         catchError(HandlerError.handleError)
       );

   }

   CloseSprint (sprintId: string, approverId: string): Observable<SprintModel> {

    const httpHeaders = {
      'Content-Type': 'application/json'
    };

     const queryStringParams
       = {
       sprintId: sprintId,
       approverId: approverId,
     };

     return this.http_.post<SprintModel>(this.baseUrl + '/Sprint/CloseSprint', '', { params: queryStringParams, headers: httpHeaders})
       .pipe(
         retry(1),
         catchError(HandlerError.handleError)
       );
   }

   ReviewCompleted (sprintId: string, approverId: string): Observable<SprintModel> {

     const httpHeaders = {
       'Content-Type': 'application/json'
     };

     const queryStringParams
       = {
       sprintId: sprintId,
       approverId: approverId
     };

     return this.http_.post<SprintModel>(this.baseUrl + '/Sprint/ReviewCompleted', '', { params: queryStringParams, headers: httpHeaders})
       .pipe(
         retry(1),
         catchError(HandlerError.handleError)
       );
   }



  getSprintReportById(sprintId: string, pageIndex, pageSize): Observable<SprintReportModel[]> {

    const httpHeaders = {
      'Content-Type': 'application/json'
    };

    const queryStringParams
      = {
      sprintId: sprintId,
      pageIndex: pageIndex,
      pageSize: pageSize
    };


    return this.http_.get<SprintReportModel[]>(this.baseUrl + '/SprintReport/GetSprintReportById', { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  getSprintByProfileId(profile: string, include: string[], pageIndex, pageSize): Observable<SprintModel[]> {

    const httpHeaders = {
      'Content-Type': 'application/json',
       'accept': 'application/json;v=1.0'
    };

    const queryStringParams
      = {
      profileId: profile,
      include: include,
      pageIndex: pageIndex,
      pageSize: pageSize
    };


    return this.http_.get<SprintModel[]>(this.baseUrl + '/Sprint/GetSprintByProfileId', { params: queryStringParams, headers: httpHeaders })
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

  reviewCheckList(sprintReportLineItem: SprintReportModel, approverId: string): Observable<SprintReportModel> {

      const httpHeaders = {
        'Content-Type': 'application/json'
      };

      const queryStringParams
        = {
        approverId: approverId,
      };

      return this.http_.post<SprintReportModel>(this.baseUrl + '/SprintReport/reviewCheckList', sprintReportLineItem, { params: queryStringParams, headers: httpHeaders})
        .pipe(
          retry(1),
          catchError(HandlerError.handleError)
        );
    }

  updateScore(unplannedTaskScoreData: UnplannedTaskScoreModel, include: string): Observable<any> {

    const httpHeaders = {
      'Content-Type': 'application/json'
    };

    const queryStringParams
      = {
      sprintId: unplannedTaskScoreData.sprintId,
        taskId: unplannedTaskScoreData.taskId,
    hours: unplannedTaskScoreData.hours,
    profileId: unplannedTaskScoreData.profileId,
        include: include
    };

    return this.http_.post<any>(this.baseUrl + '/Sprint/UpdateScore', '', { params: queryStringParams, headers: httpHeaders})
      .pipe(
        retry(1),
        catchError(HandlerError.handleError)
      );
  }

}
