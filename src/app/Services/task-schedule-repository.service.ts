import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {ServerConfigService} from '../settings/server-config.service';
import {TaskScheduleModel} from '../profile/schedule/task-schedule.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MessageBoxService} from '../settings/message-box.service';
import {AppState} from '../app.state';
import {Store} from '@ngrx/store';
import * as TaskScheduleActions from '../shared/store/task-schedule.action';
import {TaskScheduleManagementService} from './task-schedule-management.service';
import {ApiError} from '../settings/api-error.model';

// export keyword is same as public keyword in C# and Java. If export keyword is used, the class
// can used in other files.

@Injectable()
export class TaskScheduleRepositoryService {

  // Special type of Observable that can emit an event. Observables that listens to below variable would have a new ref each time an event is emiited
  private taskSchedulesInStore = new Subject<TaskScheduleModel[]>();
  // Observable string streams
  public taskSchedulesInStore$ = this.taskSchedulesInStore.asObservable();
 // Subscribing to current state of task schedules in store
  TaskScheduleInStore: TaskScheduleModel[] = [];


  constructor(private http: HttpClient,
              private serverConfigService: ServerConfigService,
              private snackBarService: MatSnackBar,
              private messageBoxService: MessageBoxService,
              private store: Store<AppState>,
              private taskScheduleManagementService: TaskScheduleManagementService,
              ) {
    this.store.select('taskSchedule').subscribe(
      {next: (taskSchedules) => {
        this.TaskScheduleInStore = taskSchedules;
      }});
  }

  // Forcefully triggering an event that would pass a new reference in local observable, hence populate local variable in the component that subscribes to it
  public readTaskScheduleInStore(profileId: string) {
    this.emitTaskScheduleInStore(this.TaskScheduleInStore.filter(x => x.owner === profileId));
  }
  // Fetch Task Schedules , Check if Schedules exist for current month , add to TaskScheduleList and store in Store Module
  public async fetchTaskSchedulesForMonthAndProfile(profileId: string, month: number, year: number): Promise<void> {
    await this.updateTaskSchedulesInStore(profileId, month, year);
    this.emitTaskScheduleInStore(this.TaskScheduleInStore.filter(x => x.owner === profileId)); }

  private async updateTaskSchedulesInStore(profileId: string, month: number, year: number) {
    // Need to check availability against following months
    const taskListForMonths = [month - 1, month, month + 1];
    for (const value of taskListForMonths) {
      if (!this.checkIfSchedulesExistForMonth( this.TaskScheduleInStore, profileId, value.toString(), year.toString())) {
        this.store.dispatch(new TaskScheduleActions.AddAllTaskSchedule( await  this.getTaskScheduleFromDb(profileId, value, year).toPromise()));
      }
    }

  }
  public removeTaskSchedule(taskScheduleId: string, profileId: string) {
    this.taskScheduleManagementService.deleteTaskScheduleById(taskScheduleId).subscribe({
      next: () => {
        this.store.dispatch(new TaskScheduleActions.RemoveTaskSchedule(taskScheduleId));
        this.emitTaskScheduleInStore(this.TaskScheduleInStore.filter(x => x.owner === profileId));
      },
      error: () => {}
    });
  }


  // Update Task Summary Id in case of new task summary Id
  public updateTaskSummaryIdOfTaskSchedule(taskScheduleId: string, profileId: string) {
    this.taskScheduleManagementService.getTaskScheduleById(taskScheduleId, 'taskSummary').subscribe({
      next: (taskSchedule) => {
        this.store.dispatch(new TaskScheduleActions.RemoveTaskSchedule(taskScheduleId));
        this.store.dispatch(new TaskScheduleActions.AddTaskSchedule(taskSchedule));
        this.emitTaskScheduleInStore(this.TaskScheduleInStore.filter(x => x.owner === profileId));
      },
      error: (apiError: ApiError) => {this.messageBoxService.info('Task summary Id not updated',
        apiError.title, apiError.detail); }
    });
  }
  // Add a Task Schedule to current task schedule
  public AddNewTaskSchedule(taskScheduleId: string, profileId: string) {
    this.taskScheduleManagementService.getTaskScheduleById(taskScheduleId).subscribe({
      next: (taskSchedule) => {
        console.log(taskSchedule);
        this.store.dispatch(new TaskScheduleActions.AddTaskSchedule(taskSchedule));
        this.emitTaskScheduleInStore(this.TaskScheduleInStore.filter(x => x.owner === profileId));
      }
    });
  }

  private checkIfSchedulesExistForMonth(taskSchedules: TaskScheduleModel[], profileId: string, month: string, year: string): boolean {
    const filteredTaskSchedule = taskSchedules.filter(function (value) {
      return (new Date(value.date).getMonth() + 1).toString() === month &&
        new Date(value.date).getFullYear().toString() === year && value.owner === profileId;
    });
    return filteredTaskSchedule.length !== 0;
  }


  private  getTaskScheduleFromDb(profileId: string, month: number, year: number): Observable<TaskScheduleModel[]> {
    return this.taskScheduleManagementService.getTaskScheduleByProfileId(profileId, month, year);
  }

  private emitTaskScheduleInStore(revisedTaskScheduleList: TaskScheduleModel[]) {
    this.taskSchedulesInStore.next(revisedTaskScheduleList);
  }



}
