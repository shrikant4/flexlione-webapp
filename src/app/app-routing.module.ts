
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SprintReportComponent} from './sprint/sprint-report/sprint-report.component';

export const routes: Routes = [

  {
      // this would help to redirect the user to login page directly when application starts
      // '' indicates default url localhost4200
      // without pathMatch router throws error. The path-match strategy 'full' matches against the entire URL. It is important to do this when redirecting empty-path routes
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'

  },

  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'task-tree',
    loadChildren: () => import('./tasks-hierarchy/task-hierarchy.module').then(m => m.TaskHierarchyModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
  },
  {
    path: 'editor',
    loadChildren: () => import('./editor/editor.module').then(m => m.EditorModule)
  },
  {
    path: 'article',
    loadChildren: () => import('./article/article.module').then(m => m.ArticleModule)
  },
  {
    path: 'template',
    loadChildren: () => import('./template-tasks/template-tasks.module').then(m => m.TemplateTasksModule)
  },
  {
    path: 'stand-up',
    loadChildren: () => import('./stand-up/stand-up.module').then(m => m.StandUpModule)
  },
  {
    path: 'sprint',
    loadChildren: () => import('./sprint/sprint.module').then(m => m.SprintModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preload all modules; optionally we could
    // implement a custom preloading strategy for just some
    // of the modules (PRs welcome 😉)
   // preloadingStrategy: PreloadAllModules,
    relativeLinkResolution: 'legacy'
})],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(
  ) {}
}
