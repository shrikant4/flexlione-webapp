import { Component, OnInit } from '@angular/core';
import { UserService } from './core';
import { Router} from '@angular/router';
import {NoAuthGuard} from './auth/no-auth-guard.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  constructor(private userService: UserService,
              public router: Router,
              authService: NoAuthGuard) {

  }

  ngOnInit() {
    this.userService.populate();
  }
}
